using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Visceral.Controllers
{
    [Route("api")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly object _apiInformations = new
        {
            title = "Visceral API",
            author = "mailto:visceral_team@protonmail.com"
        };
        
        private readonly object _apiUrls = new
        {
            labels_url = "http://localhost:8080/api/labels",
            label_outer_neighbors_labels = "http://localhost:8080/api/labels/neighbours/outer/{?label}",
            node_by_id_url = "http://localhost:8080/api/nodes/{id}{?dateStart,dateEnd}",
            nodes_url = "http://localhost:8080/api/nodes/{?label,limit,skip,dateStart,dateEnd}",
            extend_from_node_by_id_url = "http://localhost:8080/api/nodes/{id}/extend/{?label,type,limit,skip,dateStart,dateEnd}"
        };

        
        //GET /api
        
        /// <summary>
        /// Home Documents for HTTP APIs, according to the IETF draft https://tools.ietf.org/html/draft-nottingham-json-home-06 .
        /// </summary>
        /// <returns>API informations and resources URL.</returns>
        /// <response code="200">Returns API informations and resources URL.</response>
        [Route(""),HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<object> HomeDocument()
        {
            Response.ContentType = "application/vnd.api+json";
            
            return new {
                api = _apiInformations,
                resources = _apiUrls
            };
        }
    }
}