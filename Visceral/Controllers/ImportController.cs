﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Visceral.Models.ConfigFiles;
using Visceral.Models.View;
using Visceral.Repository.Import;
using Visceral.Services.Import;

namespace Visceral.Controllers
{
    public class ImportController : Controller
    {
        private readonly WindowsEventRepository _windowsEventRepository;
        private readonly PlasoMftEventRepository _plasoMftEventRepository;
        private readonly GraphRepository _graphRepository;
        private readonly IndexRepository _indexRepository;
        private readonly CustomFunctionRepository _customFunctionRepository;
        private readonly UploadConfig _uploadConfig;
        private readonly JsonSplittingService _jsonSplittingService;

        public ImportController(WindowsEventRepository windowsEventRepository, UploadConfig uploadConfig,
            PlasoMftEventRepository plasoMftEventRepository, JsonSplittingService jsonSplittingService, 
            GraphRepository graphRepository, IndexRepository indexRepository, CustomFunctionRepository customFunctionRepository )
        {
            _windowsEventRepository = windowsEventRepository;
            _plasoMftEventRepository = plasoMftEventRepository;
            _graphRepository = graphRepository;
            _uploadConfig = uploadConfig;
            _jsonSplittingService = jsonSplittingService;
            _indexRepository = indexRepository;
            _customFunctionRepository = customFunctionRepository;
        }
        
        public IActionResult Import()
        {
            var model = new ImportModel {UploadType = "shellbag"};
            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken,DisableRequestSizeLimit]        
        [RequestFormLimits(MultipartBodyLengthLimit = long.MaxValue)]//ValueLengthLimit = int.MaxValue,
        public async Task<ActionResult> Import(string uploadType, IFormFile formFile)
        {
            Enum.TryParse(uploadType, out ImportModel.UploadEnum uploadTypeAction);
            
            //Todo Throw an error instead of returning the view without information
            if (formFile.Length <= 0) return View();

            Console.WriteLine("Starting upload ...");
            var (fileName,filePath) = await FileUpload(formFile);
            Console.WriteLine("End upload ...");
            try
            {
                await _indexRepository.CreateIndexesIfNotAlreadyExisting();
                await _customFunctionRepository.CreateAllCustomsFunctionIfNotAlreadyExisting();
                
                switch (uploadTypeAction)
                {
                    case ImportModel.UploadEnum.Evtx:
                        await _windowsEventRepository.ImportFromJsonFile(filePath);
                        await _graphRepository.GenerateProtoObjectsFromWindowsEvents();
                        break;
                    case ImportModel.UploadEnum.DiskImg:
                    case ImportModel.UploadEnum.Shellbags:
                    case ImportModel.UploadEnum.MemDump:
                        throw new NotImplementedException();
                    case ImportModel.UploadEnum.Mft:
                        Console.WriteLine("Starting splitting ..."); 
                        var subFileNumber = _jsonSplittingService.SplitJsonFile(filePath, 5000);
                        Console.WriteLine("Ending splitting ...");
                        Console.WriteLine("Starting database import...");
                        await _plasoMftEventRepository.ImportFromJsonSubFiles(subFileNumber);
                        Console.WriteLine("End database import...");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            finally
            {
                DeleteTemporaryFile(fileName);
                DeleteFilesInDirectory("split/");
                DeleteFilesInDirectory("json/");
            }

            return View();
        }
        
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task<(string fileName, string filePath)> FileUpload(IFormFile formFile)
        {
            var fileName = Path.GetRandomFileName();
            var filePath = Path.Combine(_uploadConfig.StoredFilePath, fileName);

            await using (var stream = System.IO.File.Create(filePath))
            {
                await formFile.CopyToAsync(stream);
            }

            return (fileName , filePath);
        }
        
        private void DeleteTemporaryFile(string fileName)
        {
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));
            System.IO.File.Delete(_uploadConfig.StoredFilePath + fileName);
        }

        private void DeleteFilesInDirectory(string directoryPath)
        {
            if (directoryPath == null) throw new ArgumentNullException(nameof(directoryPath));
            
            var directory = new DirectoryInfo(_uploadConfig.StoredFilePath + directoryPath);
            foreach (var file in directory.GetFiles())
            {
                file.Delete(); 
            }           
        }
    }
}