using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Visceral.Repository;
using Visceral.Repository.API;
using Visceral.Services;
using Visceral.Services.API;

namespace Visceral.Controllers
{
    [Route("api/labels")]
    [ApiController]
    public class LabelsController : ControllerBase
    {
        private readonly LabelRepository _repository;
        private readonly LabelService _service;

        public LabelsController(LabelRepository repository,LabelService service)
        {
            _repository = repository;
            _service = service;
        }
        
        //GET /api/labels
        
        /// <summary>
        /// Get the labels of the Neo4J Database.
        /// </summary>
        /// <returns>An array of strings of all the labels of the Neo4J Database.</returns>
        /// <response code="200">Returns an array of strings of all the labels of the Neo4J Database.</response>
        [Route(""),HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IDictionary<string, object>> GetLabels()
        { 
            object queryResult = await _repository.GetLabel();
            return _service.ResultToApiFormat(queryResult);
        }

        //GET /api/labels/neighbours/outer/{?label}
        
        /// <summary>
        /// Get the distinct outer neighbours labels of a label. Used for navigability of the graph.
        /// </summary>
        /// <param name="label">Starting label for the research. If parameter isn't set, send result for all label types</param>
        /// <returns>An array of strings of all distinct outer label of the queried label.</returns>
        [Route("neighbours/outer/"),HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IDictionary<string, object>> GetOuterNeighboursLabel([FromQuery]string? label=null)
        {
            object queryResult;
            if (label == null)
            {
                queryResult = await _repository.GetAllNeighboursLabel();
            }
            else
            {
                queryResult = await _repository.GetOuterNeighboursLabel(label);
            } 
            return _service.ResultToApiFormat(queryResult);
        }
    }
}