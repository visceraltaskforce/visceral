using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Neo4j.Driver;
using Visceral.Models.ErrorsApi;
using Visceral.Repository.API;
using Visceral.Services.API;

namespace Visceral.Controllers
{
    [Route("api/nodes")]
    [ApiController]
    public class NodesController : ControllerBase
    {
        private readonly NodeRepository _nodeRepository;
        private readonly NodeService _nodeService;
        private readonly NodeNeighborsRepository _nodeNeighborsRepository;
        private readonly ApiService _apiService;
        private readonly RelationshipService _relationshipService;
        private readonly PathService _pathService;

        public NodesController(NodeRepository nodeRepository,NodeNeighborsRepository nodeNeighborsRepository,
             NodeService nodeService, RelationshipService relationshipService, ApiService apiService,
             PathService pathService)
        {
            _nodeRepository = nodeRepository;
            _nodeNeighborsRepository = nodeNeighborsRepository;
            _nodeService = nodeService;
            _relationshipService = relationshipService;
            _apiService = apiService;
            _pathService = pathService;
        }
        
        
        //GET /api/nodes/{?label,limit,skip,startDate,endDate}
        /// <summary>
        /// Gets all nodes. Optional filters are available.
        /// </summary>
        /// <param name="label">Filters the node by label. Only take one label.</param>
        /// <param name="limit">Maximum number of nodes returned.</param>
        /// <param name="skip">Number of nodes to skip before returning the rest.</param>
        /// <returns>A list of all nodes queried in Vis Node Format.</returns>
        /// <response code="200">Returns a list of all nodes queried in Vis Node Format.</response>
        /// <response code="422">If one or more parameters doesn't complete validation. Returns errors.</response>
        [Route(""),HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<IDictionary<string, object>> GetNodes([FromQuery] string? label=null,[FromQuery] int limit=0,
            [FromQuery] int skip=0)
        {
            try
            {
                List<INode> neo4JNodeList = await _nodeRepository.GetNodes(label,skip,limit);
                List<IDictionary<string, object>> visNodeList = neo4JNodeList.AsParallel()
                    .Select(node => _nodeService.NodeToVisNode(node))
                    .ToList();
                return _apiService.ResultToApiFormat(visNodeList);
            }
            catch (Exception exc) when (exc is InvalidAttributeError)
            {
                var error = ((AbstractApiError) exc);
                Response.StatusCode = error.StatusCode;
                return _apiService.ErrorToApiFormat(error.ToDictionary());
            }
        }
        
        //GET /api/nodes/{id}{?startDate,endDate}
        /// <summary>
        /// Gets a node by id.
        /// </summary>
        /// <param name="id">Id of the queried node.</param>
        /// <returns>The node with the given id in Vis Node Format.</returns>
        /// <response code="200">Returns the node found with the given id in Vis Node Format.</response>
        /// <response code="400">If there is no node with this id.</response>
        /// <response code="422">If the id parameter is negative. Returns errors.</response>
        [Route("{id}"),HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<IDictionary<string, object>> GetNodeById(int id)
        {
            try
            {
                INode neo4JNode = await _nodeRepository.GetNodeById(id);
                IDictionary<string, object> visNode = _nodeService.NodeToVisNode(neo4JNode);
                return _apiService.ResultToApiFormat(visNode);
            }
            catch (Exception exc) when (exc is NotFoundError || exc is InvalidAttributeError)
            {
                var error = ((AbstractApiError) exc);
                Response.StatusCode = error.StatusCode; 
                return _apiService.ErrorToApiFormat(error.ToDictionary());
            }
        }

        //GET /api/nodes/{id}/extend/{?label,type,limit,skip,startDate,endDate}
        /// <summary>
        /// Gets all nodes and relationships by extending from a node selected by id.
        /// </summary>
        /// <param name="id">id of the starting node.</param>
        /// <param name="label">Filters the nodes by label.</param>
        /// <param name="type">Filters the relationships by their type.</param>
        /// <param name="limit">Maximum number of nodes or relationships returned.</param>
        /// <param name="skip">Number of paths to skip before returning the rest.</param>
        /// <param name="startDate">Filter. Keep every element after this date</param>
        /// <param name="endDate">Filter. Keep every element before this date</param>
        /// <returns>The nodes and relationships extended in Vis node and edge format.</returns>
        /// <response code="200">Returns the nodes and relationships extended in Vis node and edge format.</response>
        /// <response code="422">If one or more parameters doesn't complete validation. Returns errors.</response>
        [Route("{id}/extend"), HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<IDictionary<string, object>> GetNeighborsNodesByLabel(int id,[FromQuery] string? label=null, 
            [FromQuery] string? type=null, [FromQuery] int limit=0,[FromQuery] int skip=0, 
            [FromQuery] long? startDate=null, [FromQuery] long? endDate=null )
        {
            try
            {
                var paths = await _nodeNeighborsRepository.FromNodeByIdExtend(id,label,type,limit,skip,startDate,endDate);

                var (relationships, nodes) = _pathService.PathToRelationshipsAndEndNodes(paths);

                List<IDictionary<string,object>> visEdges = relationships.AsParallel()
                    .Select(relationship => _relationshipService.RelationshipToVisEdge(relationship)).ToList();

                List<IDictionary<string,object>> visNodes = nodes.AsParallel()
                    .Select(node => _nodeService.NodeToVisNode(node))
                    .ToList();
                
                return _apiService.ResultToApiFormat(new Dictionary<string, object>
                {
                    {"nodes",visNodes},
                    {"edges",visEdges}
                });
            }
            catch (Exception exc) when (exc is InvalidAttributeError)
            {
                var error = ((AbstractApiError) exc);
                Response.StatusCode = error.StatusCode;
                return _apiService.ErrorToApiFormat(error.ToDictionary());
            }
        }

        //GET /api/node/{id}/neighbour/labels
        /// <summary>
        /// From the node id, gets neighbours labels without caring of the orientation of the relationship.
        /// </summary>
        /// <param name="id">id of the starting node.</param>
        /// <returns>A list of labels from the neighbour nodes</returns>
        [Route("{id}/neighbour/labels"), HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<object> GetNeighboursLabelById(int id)
        {
            var labels = await _nodeNeighborsRepository.FromIdGetNodeNeighboursLabel(id);
            return _apiService.ResultToApiFormat(labels);
        }
    }
}