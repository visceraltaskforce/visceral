﻿namespace Visceral.Models.ConfigFiles
{
    public class UploadConfig
    {
        public readonly string StoredFilePath;
        public UploadConfig()
        {
            StoredFilePath = "/tmp/upload/shared/";
        }
    }
}