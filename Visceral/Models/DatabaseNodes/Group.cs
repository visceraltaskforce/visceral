﻿namespace Visceral.Models.DatabaseNodes
{
    public class Group
    {
        public string? Sid;
        public string? Name;
    }
}