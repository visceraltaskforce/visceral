﻿namespace Visceral.Models.DatabaseNodes
{
    public class Object
    {
        public string? Sd;
        public string? Type;
        public string? NameGuid;
        public string? Name;
        public string? TypeGuid;
    }
}