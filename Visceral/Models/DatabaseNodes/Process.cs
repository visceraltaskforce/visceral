﻿using System;

namespace Visceral.Models.DatabaseNodes
{
    public class Process
    {
        public DateTime? TimeCreated;
        public string? LogonType;
        public string? EventID;
        public string? DomainName;
    }
}