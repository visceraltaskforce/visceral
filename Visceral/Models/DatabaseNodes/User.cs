﻿namespace Visceral.Models.DatabaseNodes
{
    public class User
    {
        public string? Username;
        public string? DomainName;
        public string? Sid;
    }
}