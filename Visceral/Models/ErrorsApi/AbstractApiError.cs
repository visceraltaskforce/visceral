using System;
using System.Collections.Generic;

namespace Visceral.Models.ErrorsApi
{
    public abstract class AbstractApiError : Exception
    {
        protected string Status;
        public int StatusCode;
        protected string Title;
        private string _description; 
        private readonly IDictionary<string, object> _pointer;
        protected IDictionary<string,object> BaseError;

        protected AbstractApiError()
        {
            Status = string.Empty;
            Title = string.Empty;
            _description = string.Empty; 
            _pointer = new Dictionary<string, object>();
            BaseError = new Dictionary<string, object>();
            
        }

        /**
         * Chainable method
         */
        public AbstractApiError SetErrorSource(string errorSource)
        {
            _pointer.Add("pointer",errorSource);
            return this;
        }

        /**
         * Chainable method
         */
        public AbstractApiError SetDescription(string description)
        {
            _description = description;
            return this;
        }

        public IDictionary<string, object> ToDictionary()
        {
            if (_description.Length > 0)
            {
                BaseError.Add("description",_description);
            }

            if (_pointer.ContainsKey("pointer"))
            {
                BaseError.Add("source",_pointer);
            }
            return BaseError;
        }
    }
}