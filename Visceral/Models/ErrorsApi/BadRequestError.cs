using System.Collections.Generic;
using System.Net;

namespace Visceral.Models.ErrorsApi
{
    public class BadRequestError : AbstractApiError
    {
        public BadRequestError()
        {
            Status = "400";
            StatusCode = (int) HttpStatusCode.BadRequest;
            Title = "Bad Request";
            BaseError = new Dictionary<string, object> {{"status", Status}, {"title", Title}};
        }
    }
}