using System.Collections.Generic;
using System.Net;

namespace Visceral.Models.ErrorsApi
{
    public class InternalServerError : AbstractApiError
    {

        public InternalServerError()
        {  
            Status = "500";
            StatusCode = (int) HttpStatusCode.InternalServerError;
            Title = "Internal Server Error";
            BaseError = new Dictionary<string, object> {{"status", Status}, {"title", Title}};
        }
    }
}