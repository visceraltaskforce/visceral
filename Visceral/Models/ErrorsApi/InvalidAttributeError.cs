using System.Collections.Generic;
using System.Net;

namespace Visceral.Models.ErrorsApi
{
    public class InvalidAttributeError : AbstractApiError
    {
                
        public InvalidAttributeError()
        {
            Status = "422";
            StatusCode = (int) HttpStatusCode.UnprocessableEntity;
            Title = "Invalid Attribute";
            BaseError = new Dictionary<string, object> {{"status", Status}, {"title", Title}};
        }
    }
}