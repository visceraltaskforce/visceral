using System.Collections.Generic;
using System.Net;

namespace Visceral.Models.ErrorsApi
{
    public class NotFoundError : AbstractApiError
    {
        public NotFoundError()
        {
            Status = "404";
            StatusCode = (int) HttpStatusCode.NotFound;
            Title = "Not Found";
            BaseError = new Dictionary<string, object> {{"status", Status}, {"title", Title}};
        } 
    }
}