using System.Collections.Generic;
using System.Net;

namespace Visceral.Models.ErrorsApi
{
    public class NotImplementedError : AbstractApiError
    {
        public NotImplementedError()
        { 
            Status = "501";
            StatusCode = (int) HttpStatusCode.NotImplemented;
            Title = "Not Implemented Yet";
            BaseError = new Dictionary<string, object> {{"status", Status}, {"title", Title}};
        }
    }
}