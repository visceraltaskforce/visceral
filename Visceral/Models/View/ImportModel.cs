﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Visceral.Models.View
{
    public class ImportModel
    {
        [Required]
        public string? UploadType { get; set; }

        [Required]
        public IFormFile? FormFile;
        
        public enum UploadEnum 
        {
            Evtx,
            [Display(Name="MasterFileTable")]
            Mft,
            [Display(Name="Disk image")]
            DiskImg,
            [Display(Name="Memory dump")]
            MemDump,
            Shellbags
        }
    }
}