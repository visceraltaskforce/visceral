using System.Collections.Generic;
using System.Threading.Tasks;
using Neo4j.Driver;
using Visceral.Utils;

namespace Visceral.Repository.API
{
    public class LabelRepository
    {
        private readonly IDriver _driver;
        
        public LabelRepository(IDriver driver)
        {
            _driver = driver;
        }
        
        public async Task<object> GetLabel()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                IResultCursor cursor = await new CypherQueryBuilder(session)
                    .Call("db.labels")
                    .Exec();
                List<string> labels = await cursor.ToListAsync(record => record["label"].As<string>());
                await cursor.ConsumeAsync();
                return labels;
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        /**
         * Gets all the labels available in outer neighbour nodes from nodes of a specific label
         */
        public async Task<object> GetOuterNeighboursLabel(string startingLabel)
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                IResultCursor cursor = await new CypherQueryBuilder(session)
                    .Match("(starting_node)-->(end_node)")
                    .WhereDynamicParamOptional("$Label IN labels(starting_node)", startingLabel,
                        CypherQueryBuilder.WhereOperators.And)
                    .Unwind("labels(end_node) AS EndLabel")
                    .With("DISTINCT EndLabel")
                    .Return("EndLabel")
                    .Exec();
                
                List<string> labels = await cursor.ToListAsync(record => record["EndLabel"].As<string>());
                await cursor.ConsumeAsync();
                return labels;
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        public async Task<object> GetAllNeighboursLabel()
        {
            List<string> labels = (List<string>) await GetLabel();
            IDictionary<string,List<string>> navigationDictionary = new Dictionary<string, List<string>>();

            foreach(var label in labels)
            {
                List<string> navigationList = (List<string>) await GetOuterNeighboursLabel(label);
                navigationDictionary.Add(label, navigationList);
            }

            return navigationDictionary;
        }
    }
}