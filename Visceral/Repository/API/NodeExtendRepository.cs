using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Neo4j.Driver;
using Visceral.Models.ErrorsApi;
using Visceral.Services.API;
using Visceral.Utils;

namespace Visceral.Repository.API
{
    public class NodeNeighborsRepository
    {
        private readonly IDriver _driver;
        private readonly DateService _dateService;
        
        public NodeNeighborsRepository(IDriver driver,DateService dateService)
        {
            _driver = driver;
            _dateService = dateService;
        }

        /**
         * Using apoc.path.expand
         * https://neo4j.com/docs/labs/apoc/current/graph-querying/expand-paths/
         */
        public async Task<List<IPath>> FromNodeByIdExtend(int nodeId,string? neighborsLabel,string? relationshipType,
            int limit, int skip, long? startDate,long? endDate)
        {
            if (nodeId < 0)
            {
               throw new InvalidAttributeError()
                   .SetDescription($"The id parameter must be a positive number. You gave : {nodeId}")
                   .SetErrorSource("/api/nodes/{id}/extend/"); 
            }
            if (neighborsLabel != null && !neighborsLabel.All(char.IsLetter))
            {
                throw new InvalidAttributeError()
                    .SetDescription("The label parameter must contain only letters (a-zA-Z)")
                    .SetErrorSource("/api/nodes/id/extend/{?label}"); 
            }
            if (relationshipType != null && !relationshipType.All(char.IsLetter))
            {
                throw new InvalidAttributeError()
                    .SetDescription("The type parameter must contain only letters (a-zA-Z)")
                    .SetErrorSource("/api/nodes/id/extend/{?type}");
            }

            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                var neo4JStringStartDate = DateService.MicrosecondTimestampToNeo4JDatetimeString(startDate);
                var neo4JStringEndDate = DateService.MicrosecondTimestampToNeo4JDatetimeString(endDate);
                
                IResultCursor cursor = await new CypherQueryBuilder(session)
                    .Match("paths=(sn)-[rl]-(en)")
                    .WhereDynamicParam("ID(sn) = $NodeId", nodeId, CypherQueryBuilder.WhereOperators.And)
                    .WhereDynamicParamOptional("$NeighboursLabel IN labels(en)", neighborsLabel,
                        CypherQueryBuilder.WhereOperators.And)
                    .WhereDynamicParamOptional("type(rl) = $RelationshipType", relationshipType,
                        CypherQueryBuilder.WhereOperators.And)
                    .WhereStaticOptional(
                        $"{neo4JStringStartDate} <= rl.TimeCreated",
                        neo4JStringStartDate != null,
                        CypherQueryBuilder.WhereOperators.And)
                    .WhereStaticOptional(
                        $"rl.TimeCreated <= {neo4JStringEndDate}",
                        neo4JStringEndDate != null,
                        CypherQueryBuilder.WhereOperators.And)
                    //.WhereDynamicParamOptional("$startDate <= rl.TimeCreated", 
                    //    DateService.MicrosecondTimestampToNeo4JDatetimeString(startDate),
                    //    CypherQueryBuilder.WhereOperators.And)
                    //.WhereDynamicParamOptional("rl.TimeCreated <= $endDate", 
                    //    DateService.MicrosecondTimestampToNeo4JDatetimeString(endDate), 
                    //    CypherQueryBuilder.WhereOperators.And)
                    .Unwind("paths AS Path")
                    .Return("Path")
                    .Skip(skip)
                    .Limit(limit)
                    .Exec();
                
                List<IPath> paths = await cursor.ToListAsync(record => record["Path"].As<IPath>());

                //Debugging in order to understand why {} are messing with RunAsync(string,dictionnary<string,object>)
                //var resultSummary = await cursor.ConsumeAsync();
                //Console.WriteLine(resultSummary.Query);
                return paths;

            }
            catch (InvalidOperationException) //When no record is returned
            {
               throw new NotFoundError()
                   .SetDescription($"Node with following id doesn't exist : {nodeId}");
            }
            finally
            {
                await session.CloseAsync();
            }
        }
        
        /**
         * Gets neighbours labels of a selected node by id without caring of the orientation of the relationship.
         */
        public async Task<List<string>> FromIdGetNodeNeighboursLabel(int nodeId)
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
        
            try
            {
                IResultCursor cursor = await new CypherQueryBuilder(session)
                    .Match("(starting_node)--(end_node)")
                    .WhereDynamicParam("ID(starting_node) = $NodeId", nodeId,
                        CypherQueryBuilder.WhereOperators.And)
                    .Unwind("labels(end_node) AS EndLabel")
                    .With("DISTINCT EndLabel")
                    .Return("EndLabel")
                    .Exec();
        
                List<string> labels = await cursor.ToListAsync(record => record["EndLabel"].As<string>());
                await cursor.ConsumeAsync();
                return labels;
            }
            finally
            {
                await session.CloseAsync();
            }
        }
    }
}