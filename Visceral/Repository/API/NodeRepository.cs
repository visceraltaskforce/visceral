using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Neo4j.Driver;
using Visceral.Models.ErrorsApi;
using Visceral.Utils;

namespace Visceral.Repository.API
{
    public class NodeRepository
    {
        private readonly IDriver _driver;
        
        public NodeRepository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task<INode> GetNodeById(int nodeId)
        {
            if (nodeId < 0) //Checking if nodeId isn't negative 
            {
                throw new InvalidAttributeError()
                    .SetDescription($"The id parameter must be a positive number. You gave : {nodeId}")
                    .SetErrorSource("/api/nodes/{id}");
            }

            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                IResultCursor cursor = await new CypherQueryBuilder(session)
                    .Match("(node)")
                    .WhereDynamicParam("ID(node) = $NodeId",nodeId, CypherQueryBuilder.WhereOperators.And)
                    .Return("node AS Node")
                    .Exec();
                
                return await cursor.SingleAsync(record => record["Node"].As<INode>());
            }
            catch (InvalidOperationException) //When no record is returned
            {
               throw new NotFoundError()
                   .SetDescription($"Node with following id doesn't exist : {nodeId}");
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        public async Task<List<INode>> GetNodes(string? label, int skip, int limit)
        {
            if (label != null && !label.All(char.IsLetter))
            {
                throw new InvalidAttributeError()
                    .SetDescription("The label parameter must contain only letters (a-zA-Z)")
                    .SetErrorSource("/api/nodes/label/{label}");
            }

            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                IResultCursor cursor = await new CypherQueryBuilder(session)
                    .Match("(node)")
                    .WhereDynamicParamOptional("$Label IN labels(node)",label,
                        CypherQueryBuilder.WhereOperators.And)
                    .Return("node AS Node")
                    .Skip(skip)
                    .Limit(limit)
                    .Exec();
                
                List<INode> nodes = await cursor.ToListAsync(record => record["Node"].As<INode>());
                await cursor.ConsumeAsync();
                return nodes;
            }
            finally
            {
                await session.CloseAsync();
            }
        }
    }
}