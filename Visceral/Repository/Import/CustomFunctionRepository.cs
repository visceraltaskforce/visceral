﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import
{
    public class CustomFunctionRepository
    {
        private readonly IDriver _driver;

        public CustomFunctionRepository(IDriver driver)
        {
            _driver = driver;
        }

        /**
         * Creates all the custom functions useful for the database requests
         */
        public async Task CreateAllCustomsFunctionIfNotAlreadyExisting()
        {
            await CreateCustomFunctionIfNotAlreadyExisting("date_EvtxToNeo4J",
                "WITH split($evtx_date,' ') AS date_split " +
                "WITH split(date_split[0],'-') AS d, split(date_split[1],'.') AS t " +
                "WITH toInteger(d[0]) as y, toInteger(d[1]) as m, toInteger(d[2]) as d, split(t[0],':') as time, toInteger(t[1]) as mics " +
                "WITH y,m,d,toInteger(time[0]) as h, toInteger(time[1]) as mn, toInteger(time[2]) as s, mics " +
                "RETURN datetime({year:y,month:m,day:d,hour:h,minute:mn,second:s,microsecond:mics})",
                "DATETIME",
                new List<(string, string)>{("evtx_date","STRING")},
                "Transforms a date in string format from an Evtx to a Datetime object in Neo4J");
        }

        /**
         * Creates a custom function with apoc in the database if it doesn't already exist
         */
        private async Task CreateCustomFunctionIfNotAlreadyExisting(string functionName, string functionQuery, string returnType,
            List<(string, string)> functionParameters, string functionDescription)
        {
            if (functionName == null) throw new ArgumentNullException(nameof(functionName));
            if (functionQuery == null) throw new ArgumentNullException(nameof(functionQuery));
            if (returnType == null) throw new ArgumentNullException(nameof(returnType));
            if (functionParameters == null) throw new ArgumentNullException(nameof(functionParameters));
            if (functionDescription == null) throw new ArgumentNullException(nameof(functionDescription));
            if (functionParameters.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(functionParameters));

            if (await FunctionAlreadyExists(functionName))
            {
                Console.WriteLine("exists");
                return;
            }
            
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                var query = $"CALL apoc.custom.asFunction(\"{functionName}\",\"{functionQuery}\",\"{returnType}\",[";
                foreach (var (paramName, paramValue) in functionParameters)
                {
                    query += $"[\"{paramName}\",\"{paramValue}\"],";
                }
                query = query.Remove(query.Length - 1) + $"],false,\"{functionDescription}\")"; //Removing the last coma and adding closing bracket/parenthese

                IResultCursor cursor = await session.RunAsync(query);
                await cursor.ConsumeAsync();
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        /**
         * Checks if the custom function already exists in the database
         */
        private async Task<bool> FunctionAlreadyExists(string functionName)
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                const string query = "CALL apoc.custom.list";
                IResultCursor cursor = await session.RunAsync(query);
                List<string> customs = await cursor.ToListAsync(record => record["name"].As<string>());
                await cursor.ConsumeAsync();
                
                //No type verification ! Process could have same name as the function !
                return customs.Contains(functionName);
            }
            finally
            {
                await session.CloseAsync();
            }
        }
    }
}