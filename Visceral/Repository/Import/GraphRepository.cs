﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Neo4j.Driver;
using Visceral.Repository.Import.WindowsEvents;

namespace Visceral.Repository.Import
{
    public class GraphRepository
    {
        private readonly IDriver _driver;
        
        public GraphRepository(IDriver driver)
        {
            _driver = driver;
        }
        
        /**
         * Generates the pre-analyzed graph from WindowsEvents in Neo4J database
         */
        public async Task GenerateProtoObjectsFromWindowsEvents()
        {
            // Generate the elements of the pre-analyzed graph (all implementing the interface IEventWindowsSpecificRepository)
            var windowsEventRepositoryType = typeof(IEventWindowsSpecificRepository);
            var windowsEventRepositoryInstance = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => windowsEventRepositoryType.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .Select(x => (IEventWindowsSpecificRepository?) Activator.CreateInstance(x, _driver))
                .ToList();
            
            foreach (var windowsEventRepository in windowsEventRepositoryInstance)
            {
                if (windowsEventRepository != null)
                {
                    await windowsEventRepository.Populate();
                }
            }
        }
    }
}