﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import
{
    public class IndexRepository
    {
        private readonly IDriver _driver;
        
        public IndexRepository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task CreateIndexesIfNotAlreadyExisting()
        { 
            //Pre-Analyzed Graph Indexes
            await CreateIndex("SessionLogonId","LogonId","Session"); 
            await CreateIndex("ComputerName","Name","Computer"); 
            await CreateIndex("HandleId","Id","Handle"); 
            await CreateIndex("ObjectName","Name","Object"); 
            await CreateIndex("GroupSid","Sid","Group");
            await CreateCompositeIndex("UserNameDomainName",new List<string> {"Name", "DomainName"},"Process");
            await CreateCompositeIndex("ProcessIdName",new List<string> {"Name", "Id"},"Process");
            
            //Import Indexes
            await CreateIndex("WindowsEventId","EventRecordID","WindowsEvent");
            await CreateCompositeIndex("PlasoMftEventId", new List<string> {"EventId","pathspec.parent.parent.parent.location"}, "PlasoMftEvent");
        }

        /**
         * Creates a simple index in the Neo4J database
         */
        private async Task CreateIndex( string indexName, string index, string nodeLabel)
        {
            if (index == null) throw new ArgumentNullException(nameof(index));
            if (indexName == null) throw new ArgumentNullException(nameof(indexName));
            if (nodeLabel == null) throw new ArgumentNullException(nameof(nodeLabel));
            if (index == string.Empty || indexName == string.Empty || nodeLabel == string.Empty)
            {
                throw new ArgumentException("Parameters of CreateIndex should not be empty");
            }
            
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                // If index already exist, end the function
                if (await IndexAlreadyExists(indexName)) return;
                
                // Create the index
                var creationQuery = $"CREATE INDEX {indexName} FOR (n:{nodeLabel}) ON (";
                creationQuery += $"n.{index})";
                IResultCursor cursor2 = await session.RunAsync(creationQuery);
                await cursor2.ConsumeAsync();
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        /**
         * Creates a composite index (index based on more than one parameter) in the Neo4J database
         */
        private async Task CreateCompositeIndex(string indexName, List<string> indexes, string nodeLabel)
        {
            if (indexes == null) throw new ArgumentNullException(nameof(indexes));
            if (indexName == null) throw new ArgumentNullException(nameof(indexName));
            if (nodeLabel == null) throw new ArgumentNullException(nameof(nodeLabel));
            if(indexes.Count < 2) throw new ArgumentException("Indexes must at least contain 2 strings");
            if (indexName == string.Empty || nodeLabel == string.Empty)
            {
                throw new ArgumentException("String parameters of CreateCompositeIndex should not be empty");
            }

            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            try
            {
                // If index already exist, end the function
                if (await IndexAlreadyExists(indexName)) return;
                
                // Create the composite index
                var creationQuery = $"CREATE INDEX {indexName} FOR (n:{nodeLabel}) ON (";
                var lastIndex = indexes.Count - 1;
                indexes.ForEach(index =>
                {
                    if (indexes.IndexOf(index) == lastIndex)
                    {
                        creationQuery += $"n.`{index}`)";
                    }
                    else creationQuery += $"n.`{index}`,";
                }); 
                IResultCursor cursor2 = await session.RunAsync(creationQuery);
                await cursor2.ConsumeAsync();
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        /**
         * Checks if the index exist in the Neo4J database, based on its name.
         */
        private async Task<bool> IndexAlreadyExists(string indexName)
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            
            // Getting the list of created indexes in Neo4J
            const string query = "CALL db.indexes";
            IResultCursor cursor = await session.RunAsync(query);
            List<string> existingIndexes = await cursor.ToListAsync(record => record["name"].As<string>());
            await cursor.ConsumeAsync();

            return existingIndexes.Contains(indexName);
        }
    }
}