﻿using System;
using System.Threading.Tasks;
using Neo4j.Driver;
using Visceral.Models.ConfigFiles;

namespace Visceral.Repository.Import
{
    public class PlasoMftEventRepository
    {
        private readonly UploadConfig _uploadConfig;
        private readonly IDriver _driver;
        
        public PlasoMftEventRepository(IDriver driver,UploadConfig uploadConfig)
        {
            _uploadConfig = uploadConfig;
            _driver = driver;
        }
        
        public async Task ImportFromJsonSubFiles(int subFileNumber)
        {

            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));

            try
            {
                await CreatePlasoMftEventsFromSubFiles(session, subFileNumber);
            }
            finally
            {
                await session.CloseAsync();
            }
        }
        
        private async Task CreatePlasoMftEventsFromSubFiles(IAsyncSession session, int subFileNumber)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));
            if (subFileNumber <= 0) throw new ArgumentOutOfRangeException(nameof(subFileNumber));

            var query =
                "CALL apoc.periodic.iterate(\""+ 
                $"UNWIND range(0, {subFileNumber-1}) as subFileNb return subFileNb " +
                "\",\""+
                $"CALL apoc.load.json('file://{_uploadConfig.StoredFilePath}json/import'+substring('000000',size(subFileNb+'')) + subFileNb +'.json') "+
                "YIELD value AS events_list "+
                "UNWIND events_list AS event "+
                "UNWIND event AS evt_row "+
                "CREATE (e:PlasoMftEvent) "+
                "SET e += evt_row "+
                "RETURN e"+
                "\", {batchSize:1, parallel:false});" ;
                        
            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }
    }
}