﻿using System;
using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import
{
    public class WindowsEventRepository 
    {
        private readonly IDriver _driver;
        public WindowsEventRepository(IDriver driver)
        {
            _driver = driver;

        }

        public async Task ImportFromJsonFile(string jsonFilePath)
        {
            if (jsonFilePath == null) throw new ArgumentNullException(nameof(jsonFilePath));

            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));

            try
            {
                await CreateWindowsEvents(session, jsonFilePath);
                await AddWindowsEventDynamicData(session, jsonFilePath);
                await CleanEventProviderName(session);
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        private async Task CreateWindowsEvents(IAsyncSession session,string jsonFilePath)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));
            if (jsonFilePath == null) throw new ArgumentNullException(nameof(jsonFilePath));

            var queryEventSystemCreation =
                "CALL apoc.periodic.iterate(\""+ 
                $"CALL apoc.load.json('file://{jsonFilePath}') " +
                "YIELD value " +
                "WITH value AS json " +
                "UNWIND json.evtxtract as event " +
                "UNWIND event['Event'] as eventContent " +
                "WITH eventContent['System'] as sysInfo " +
                "RETURN sysInfo.EventRecordID['#text'] as eventRecordID, sysInfo.Task['#text'] as task, " +
                "sysInfo.Keywords['#text'] as keywords, sysInfo.Channel['#text'] as channel, sysInfo.Provider['@Name'] as provider, " +
                "sysInfo.Provider['@Guid'] as providerGUID, sysInfo.Security['@UserID'] as security, " +
                "sysInfo.TimeCreated['@SystemTime'] as timeCreated, sysInfo.Computer['#text'] as computer, " +
                "sysInfo.EventID['#text'] as eventID, sysInfo.Level['#text'] as level " +
                "\",\""+
                "MERGE (e:WindowsEvent { " +
                "EventRecordID : eventRecordID,  " +
                "Task : task, " +
                "Keywords : keywords, " +
                "Channel : channel, " +
                "Provider : provider, " +
                "Security : security, " +
                "TimeCreated : timeCreated, " +
                "Computer : computer, " +
                "EventID : eventID, " +
                "Level : level " +
                "}) " +
                "RETURN e.EventRecordID AS eventRecordId "+
                "\", {batchSize:1000, parallel:true, iterateList: true});" ;
            
            IResultCursor cursor = await session.RunAsync(queryEventSystemCreation);
            await cursor.ConsumeAsync();
        }

        private async Task AddWindowsEventDynamicData(IAsyncSession session, string jsonFilePath)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));
            if (jsonFilePath == null) throw new ArgumentNullException(nameof(jsonFilePath));

            var queryAddingEventData =
                "CALL apoc.periodic.iterate(\"" +
                $"CALL apoc.load.json('file://{jsonFilePath}') " +
                "YIELD value " +
                "WITH value AS json " +
                "UNWIND json.evtxtract as event " +
                "UNWIND event.Event as eventContent " +
                "UNWIND eventContent.EventData as eventData " +
                "UNWIND eventData.Data as rawdata " +
                "WITH eventContent.System.EventRecordID['#text'] as eventRecordID, apoc.map.fromValues([rawdata['@Name'], rawdata['#text']]) as mapdata " +
                "WITH eventRecordID, apoc.map.mergeList(collect(mapdata)) as listMapData " +
                "UNWIND listMapData as row " +
                "RETURN eventRecordID, row"+
                "\",\""+
                "MATCH (e:WindowsEvent { EventRecordID: eventRecordID }) " +
                "SET e += row " +
                "RETURN e.EventRecordID AS eventRecordId "+
                "\", {batchSize: 10000, iterateList: true});" ;
                            
            IResultCursor cursor = await session.RunAsync(queryAddingEventData);
            await cursor.ConsumeAsync();
        }

        private async Task CleanEventProviderName(IAsyncSession session)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));
            
            const string query = 
                "MATCH (n:WindowsEvent) "+
                "SET n.Provider = replace(n.Provider,'\\\\','') "+
                "RETURN DISTINCT n.Provider";
            
            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }
    }
}