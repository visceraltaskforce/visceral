﻿using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4616Repository : IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4616Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await ProcessAsUser();
            await UserUsingSession();
            await SessionChangeTimeComputer();
        }
        
        private async Task ProcessAsUser()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query = 
                "MATCH (n:WindowsEvent {EventID : \"4616\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
                "WHERE n.ProcessId IS NOT NULL AND n.SubjectUserSid IS NOT NULL "+
                "MERGE (process:Process {Id : n.ProcessId}) "+
                "ON CREATE SET "+
                "process.Name = n.ProcessName, "+
                "process.Id = n.ProcessId "+
                "MERGE (subjectUser:User { Sid : n.SubjectUserSid }) "+
                "ON CREATE SET "+
                "subjectUser.Sid = n.SubjectUserSid, "+
                "subjectUser.Username = n.SubjectUserName, "+
                "subjectUser.DomainName = n.SubjectDomainName "+
                "WITH n,subjectUser,process,n.TimeCreated as date "+
                "CREATE (process)-[processAs:AS]->(subjectUser) "+
                "SET "+
                "processAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "processAs.LogonType = n.LogonType, "+
                "processAs.EventID = n.EventID "+
                "RETURN processAs"; 
            
            IResultCursor cursor = await session.RunAsync(query); 
            await cursor.ConsumeAsync();
        }
        
        private async Task UserUsingSession()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query = 
               "MATCH (n:WindowsEvent {EventID : \"4616\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "WHERE n.SubjectLogonId IS NOT NULL AND n.SubjectUserSid IS NOT NULL "+
               "MERGE (subjectUser:User { Sid : n.SubjectUserSid }) "+
               "ON CREATE SET "+
               "subjectUser.Sid = n.SubjectUserSid, "+
               "subjectUser.Username = n.SubjectUserName, "+
               "subjectUser.DomainName = n.SubjectDomainName "+
               "MERGE (subjectSession:Session { LogonId : n.SubjectLogonId}) "+
               "ON CREATE SET "+
               "subjectSession.LogonId = n.SubjectLogonId "+
               "WITH n,subjectUser,subjectSession,n.TimeCreated as date "+
               "CREATE (subjectUser)-[usingSession:USING]->(subjectSession) "+
               "SET "+
               "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "usingSession.LogonType = n.LogonType, "+
               "usingSession.EventID = n.EventID "+
               "RETURN usingSession";

           IResultCursor cursor = await session.RunAsync(query); 
           await cursor.ConsumeAsync();
        }

        private async Task SessionChangeTimeComputer()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =   
              "MATCH (n:WindowsEvent {EventID : \"4616\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "WHERE n.SubjectLogonId IS NOT NULL AND n.Computer IS NOT NULL "+
              "MERGE (computer:Computer { Name : n.Computer}) "+
              "ON CREATE SET "+
              "computer.Name = n.Computer "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "WITH n,computer,subjectSession,n.TimeCreated as date "+
              "CREATE (subjectSession)-[changeTime:CHANGE_TIME {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(computer) "+
              "SET "+
              "changeTime.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "changeTime.EventID = n.EventID, "+
              "changeTime.NewTime = n.NewTime, "+
              "changeTime.PreviousTime = n.PreviousTime "+
              "RETURN changeTime";

           IResultCursor cursor = await session.RunAsync(query); 
           await cursor.ConsumeAsync();
        }
    }
}