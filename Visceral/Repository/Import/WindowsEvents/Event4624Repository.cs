using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4624Repository : IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4624Repository(IDriver driver)
        {
            _driver = driver;
        }

       public async Task Populate()
       {
           await ProcessAsUser();
           await UserUsingSession();
           await SessionLogOnComputer();
           await ComputerLogOnAsUser();
           await UserLogOnSession();
       }

       private async Task ProcessAsUser()
       {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query = 
             "MATCH (n:WindowsEvent {EventID : \"4624\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
             "MERGE (process:Process {Id : n.ProcessId}) "+
             "ON CREATE SET "+
             "process.Name = n.ProcessName, "+
             "process.Id = n.ProcessId "+
             "MERGE (subjectUser:User { Sid : n.SubjectUserSid }) "+
             "ON CREATE SET "+
             "subjectUser.Sid = n.SubjectUserSid, "+
             "subjectUser.Username = n.SubjectUserName, "+
             "subjectUser.DomainName = n.SubjectDomainName "+
             "WITH n,subjectUser,process,n.TimeCreated as date "+
             "CREATE (process)-[processAs:AS]->(subjectUser) "+
             "SET "+
             "processAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
             "processAs.LogonType = n.LogonType, "+
             "processAs.EventID = n.EventID "+
             "RETURN processAs";

           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
       }

       private async Task UserUsingSession()
       {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query = 
              "MATCH (n:WindowsEvent {EventID : \"4624\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectUser:User { Sid : n.SubjectUserSid }) "+
              "ON CREATE SET "+
              "subjectUser.Sid = n.SubjectUserSid, "+
              "subjectUser.Username = n.SubjectUserName, "+
              "subjectUser.DomainName = n.SubjectDomainName "+
              "MERGE (subjectSession:Session { LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "WITH n,subjectUser,subjectSession,n.TimeCreated as date "+
              "CREATE (subjectUser)-[usingSession:USING]->(subjectSession)  "+
              "SET "+
              "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "usingSession.LogonType = n.LogonType, "+
              "usingSession.EventID = n.EventID "+
              "RETURN usingSession";
       
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
       }

       private async Task SessionLogOnComputer()
       {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4624\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectSession:Session { LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "MERGE (computer:Computer { Name : n.Computer}) "+
              "ON CREATE SET "+
              "computer.Name = n.Computer "+
              "WITH n,computer,subjectSession,n.TimeCreated as date "+
              "CREATE (subjectSession)-[logonComputer:CONNECT]->(computer) "+
              "SET "+
              "logonComputer.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "logonComputer.LogonType = n.LogonType, "+
              "logonComputer.EventID = n.EventID, "+
              "logonComputer.IpAddress = n.IpAddress, "+
              "logonComputer.IpPort = n.IpPort "+
              "RETURN logonComputer";
              
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
       }

       private async Task ComputerLogOnAsUser()
       {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
               "MATCH (n:WindowsEvent {EventID : \"4624\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "MERGE (computer:Computer { Name : n.Computer}) "+
               "ON CREATE SET "+
               "computer.Name = n.Computer "+
               "MERGE (targetUser:User { Sid : n.TargetUserSid }) "+
               "ON CREATE SET "+
               "targetUser.Sid = n.TargetUserSid, "+
               "targetUser.Username = n.TargetUserName, "+
               "targetUser.DomainName = n.TargetDomainName "+
               "WITH n,computer,targetUser,n.TimeCreated as date "+
               "CREATE (computer)-[logonAs:LOG_ON_AS]->(targetUser) "+
               "SET "+
               "logonAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "logonAs.LogonType = n.LogonType, "+
               "logonAs.EventID = n.EventID "+
               "RETURN logonAs";
              
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
       }

       private async Task UserLogOnSession()
       {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
               "MATCH (n:WindowsEvent {EventID : \"4624\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "MERGE (targetSession:Session { LogonId : n.TargetLogonId}) "+
               "ON CREATE SET "+
               "targetSession.LogonId = n.TargetLogonId "+
               "MERGE (targetUser:User { Sid : n.TargetUserSid }) "+
               "ON CREATE SET "+
               "targetUser.Sid = n.TargetUserSid, "+
               "targetUser.Username = n.TargetUserName, "+
               "targetUser.DomainName = n.TargetDomainName "+
               "WITH n,targetSession,targetUser,n.TimeCreated as date "+
               "CREATE (targetUser)-[logonSession:LOG_ON]->(targetSession) "+
               "SET "+
               "logonSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "logonSession.LogonType = n.LogonType, "+
               "logonSession.EventID = n.EventID "+
               "RETURN logonSession";

           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
       }
    }
}
