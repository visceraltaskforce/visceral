using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4634Repository: IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4634Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await UserLogOffAsComputer();
            await SessionLogOffUser();
        }

        private async Task UserLogOffAsComputer()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
               "MATCH (n:WindowsEvent {EventID : \"4634\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "MERGE (computer:Computer { Name : n.Computer}) "+
               "ON CREATE SET "+
               "computer.Name = n.Computer "+
               "MERGE (targetUser:User { Sid : n.TargetUserSid }) "+
               "ON CREATE SET "+
               "targetUser.Sid = n.TargetUserSid, "+
               "targetUser.Username = n.TargetUserName, "+
               "targetUser.DomainName = n.TargetDomainName "+
               "WITH n,computer,targetUser,n.TimeCreated as date "+
               "CREATE (computer)<-[logoffAs:LOG_OFF_AS]-(targetUser) "+
               "SET "+
               "logoffAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "logoffAs.LogonType = n.LogonType, "+
               "logoffAs.EventID = n.EventID "+
               "RETURN logoffAs";

           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task SessionLogOffUser()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
               "MATCH (n:WindowsEvent {EventID : \"4634\", Provider : \"Microsoft-Windows-Security-Auditing\"}) " +
               "MERGE (targetSession:Session { LogonId : n.TargetLogonId}) " +
               "ON CREATE SET " +
               "targetSession.LogonId = n.TargetLogonId " +
               "MERGE (targetUser:User { Sid : n.TargetUserSid }) " +
               "ON CREATE SET " +
               "targetUser.Sid = n.TargetUserSid, " +
               "targetUser.Username = n.TargetUserName, " +
               "targetUser.DomainName = n.TargetDomainName "+
               "WITH n,targetSession,targetUser,n.TimeCreated as date " +
               "CREATE (targetUser)<-[logoffSession:LOG_OFF]-(targetSession) " +
               "SET " +
               "logoffSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "logoffSession.LogonType = n.LogonType, " +
               "logoffSession.EventID = n.EventID " +
               "RETURN logoffSession";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }
    }
}
