﻿using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4648Repository : IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4648Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await ProcessUsingSession();
            await SessionLogAsUser();
            await UserLogOnComputer();
        } 
        
        private async Task ProcessUsingSession()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query =
                "MATCH (n:WindowsEvent {EventID : \"4648\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
                "MERGE (p:Process {Id : n.ProcessId}) "+
                "ON CREATE SET  "+
                "p.Name = n.ProcessName, "+
                "p.Id = n.ProcessId, "+
                "p.StartedBy = n.SubjectUserName, "+
                "p.StartedBySid = n.SubjectUserSid, "+
                "p.StartedByDomain = n.SubjectDomainName "+
                "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
                "ON CREATE SET  "+
                "subjectSession.Computer = n.SubjectLogonId "+
                "WITH n,p,subjectSession,n.TimeCreated as date "+
                "CREATE (p)-[usingSession:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(subjectSession) "+
                "SET "+
                "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "usingSession.DomainName = n.SubjectDomainName, "+ 
                "usingSession.EventID = n.EventID "+
                "RETURN usingSession";

            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }

        private async Task SessionLogAsUser()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query = 
                "MATCH (n:WindowsEvent {EventID : \"4648\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
                "MERGE (targetUser:User {Username : n.TargetUserName}) "+
                "ON CREATE SET  "+
                "targetUser.Username = n.TargetUserName, "+
                "targetUser.DomainName = n.TargetDomainName "+
                "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
                "ON CREATE SET  "+
                "subjectSession.LogonId = n.SubjectLogonId "+
                "WITH n,targetUser,subjectSession,n.TimeCreated as date "+
                "CREATE (subjectSession)-[logAs:LOG_AS {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(targetUser) "+
                "SET "+
                "logAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "logAs.EventID = n.EventID "+
                "RETURN logAs";
            
            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }

        private async Task UserLogOnComputer()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query = 
                "MATCH (n:WindowsEvent {EventID : \"4648\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
                "MERGE (targetUser:User {Username : n.TargetUserName}) "+
                "ON CREATE SET  "+
                "targetUser.Username = n.TargetUserName, "+
                "targetUser.DomainName = n.TargetDomainName "+
                "MERGE (targetServer:Computer {Name : n.TargetServerName}) "+
                "ON CREATE SET  "+
                "targetServer.Name = n.TargetServerName "+
                "WITH n,targetUser,targetServer,n.TimeCreated as date "+
                "CREATE (targetUser)-[logOn:LOG_ON {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(targetServer) "+
                "SET "+
                "logOn.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "logOn.Domain = n.TargetDomainName, "+
                "logOn.IpAddress = n.IpAddress, "+
                "logOn.IpPort = n.IpPort, "+
                "logOn.EventID = n.EventID "+
                "RETURN logOn";
            
            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }
    }
}