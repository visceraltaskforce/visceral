using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4661Repository: IEventWindowsSpecificRepository
    {
       private readonly IDriver _driver;
       
       public Event4661Repository(IDriver driver)
       {
          _driver = driver;
       }

        public async Task Populate()
        {
           await ProcessAsUser();
           await UserUsingSession();
           await SessionUsingHandle();
           await HandleRequestObject();
        }

        private async Task ProcessAsUser()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4661\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectUser:User {Sid : n.SubjectUserSid}) "+
              "ON CREATE SET  "+
              "subjectUser.Username = n.SubjectUserName, "+
              "subjectUser.Sid = n.SubjectUserSid, "+
              "subjectUser.DomainName = n.SubjectDomainName "+
              "MERGE (process:Process {Id : n.ProcessId}) "+
              "ON CREATE SET  "+
              "process.Id = n.ProcessId, "+
              "process.Name = n.ProcessName "+
              "WITH n,subjectUser,process,n.TimeCreated as date "+
              "CREATE (process)-[processAs:AS {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(targetServer) "+
              "SET "+
              "processAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "processAs.EventID = n.EventID "+
              "RETURN processAs";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task UserUsingSession()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4661\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectUser:User {Sid : n.SubjectUserSid}) "+
              "ON CREATE SET  "+
              "subjectUser.Username = n.SubjectUserName, "+
              "subjectUser.Sid = n.SubjectUserSid, "+
              "subjectUser.DomainName = n.SubjectDomainName "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "WITH n,subjectSession,subjectUser,n.TimeCreated as date "+
              "CREATE (subjectUser)-[usingSession:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(subjectSession) "+
              "SET "+
              "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "usingSession.EventID = n.EventID "+
              "RETURN usingSession";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task SessionUsingHandle()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4661\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "MERGE (handle:Handle {Id : n.HandleId}) "+
              "ON CREATE SET  "+
              "handle.Id = n.HandleId "+
              "WITH n,subjectSession,handle,n.TimeCreated as date "+
              "CREATE (subjectSession)-[usingHandle:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(handle) "+
              "SET "+
              "usingHandle.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "usingHandle.EventID = n.EventID "+
              "RETURN usingHandle";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task HandleRequestObject()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4661\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (handle:Handle {Id : n.HandleId}) "+
              "ON CREATE SET  "+
              "handle.Id = n.HandleId "+
              "MERGE (object:Object {Name : n.ObjectName}) "+
              "ON CREATE SET  "+
              "object.Name = n.ObjectName, "+
              "object.Type = n.ObjectType "+
              "WITH n,object,handle,n.TimeCreated as date "+
              "CREATE (handle)-[requestObjectHandle:REQUEST {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(object) "+
              "SET "+
              "requestObjectHandle.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "requestObjectHandle.EventID = n.EventID, "+
              "requestObjectHandle.Properties = n.Properties, "+
              "requestObjectHandle.AccessMask = n.AccessMask, "+
              "requestObjectHandle.PrivilegeList = n.PrivilegeList, "+
              "requestObjectHandle.RestrictedSidCount = n.RestrictedSidCount, "+
              "requestObjectHandle.AccessList = n.AccessList "+
              "RETURN requestObjectHandle";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }
    }
}
