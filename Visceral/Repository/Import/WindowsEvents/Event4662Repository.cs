using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4662Repository: IEventWindowsSpecificRepository
    {
       private readonly IDriver _driver;
       
       public Event4662Repository(IDriver driver)
       {
          _driver = driver;
       }

        public async Task Populate()
        {
           await UserUsingSession();
           await SessionUsingHandle();
           await HandleAccessObject();
        }

        private async Task UserUsingSession()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4662\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectUser:User {Sid : n.SubjectUserSid}) "+
              "ON CREATE SET  "+
              "subjectUser.Username = n.SubjectUserName, "+
              "subjectUser.Sid = n.SubjectUserSid, "+
              "subjectUser.DomainName = n.SubjectDomainName "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "WITH n,subjectSession,subjectUser,n.TimeCreated as date "+
              "CREATE (subjectUser)-[usingSession:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(subjectSession) "+
              "SET "+
              "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "usingSession.EventID = n.EventID "+
              "RETURN usingSession";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task SessionUsingHandle()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4662\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "MERGE (handle:Handle {Id : n.HandleId}) "+
              "ON CREATE SET  "+
              "handle.Id = n.HandleId "+
              "WITH n,subjectSession,handle,n.TimeCreated as date "+
              "CREATE (subjectSession)-[usingHandle:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(handle) "+
              "SET "+
              "usingHandle.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "usingHandle.EventID = n.EventID "+
              "RETURN usingHandle";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task HandleAccessObject()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4662\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "MERGE (handle:Handle {Id : n.HandleId}) "+
              "ON CREATE SET  "+
              "handle.Id = n.HandleId "+
              "MERGE (objectAccessed:Object {NameGuid : n.ObjectName}) "+
              "ON CREATE SET  "+
              "objectAccessed.NameGuid = n.ObjectName, "+
              "objectAccessed.TypeGuid = n.ObjectType "+
              "WITH n,objectAccessed,handle,n.TimeCreated as date "+
              "CREATE (handle)-[accesingObject:ACCESS {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(objectAccessed) "+
              "SET "+
              "accesingObject.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "accesingObject.EventID = n.EventID, "+
              "accesingObject.AdditionalInfo = n.AdditionalInfo, "+
              "accesingObject.AccessMask = n.AccessMask, "+
              "accesingObject.OperationType = n.OperationType, "+
              "accesingObject.AccessList = n.AccessList "+
              "RETURN accesingObject";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }
    }
}
