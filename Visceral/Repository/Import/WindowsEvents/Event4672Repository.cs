﻿using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4672Repository : IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4672Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await ComputerLogOnWithPrivilegesSession();
        }

        private async Task ComputerLogOnWithPrivilegesSession()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query = 
               "MATCH (n:WindowsEvent {EventID : \"4672\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "WITH n, n.TimeCreated as date "+
               "MATCH (computer:Computer)-[logonSession:LOG_ON_WITH_PRIVILEGES {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(targetSs:Session {LogonId : n.SubjectLogonId}) "+
               "WITH n,computer,logonSession,targetSs,date "+
               "SET logonSession.PrivilegeList = split(n.PrivilegeList, \"			\") "+
               "RETURN DISTINCT logonSession.PrivilegeList ";
            
            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }
    }
}