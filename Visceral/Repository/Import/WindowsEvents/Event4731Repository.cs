using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4731Repository: IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4731Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await UserUsingSession();
            await SessionCreateGroupUser();
        }

        private async Task UserUsingSession()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
               "MATCH (n:WindowsEvent {EventID : \"4731\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "WHERE n.SubjectUserSid IS NOT NULL "+
               "MERGE (subjectUser:User {Sid : n.SubjectUserSid}) "+
               "ON CREATE SET  "+
               "subjectUser.Username = n.SubjectUserName, "+
               "subjectUser.Sid = n.SubjectUserSid, "+
               "subjectUser.DomainName = n.SubjectDomainName "+
               "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
               "ON CREATE SET  "+
               "subjectSession.LogonId = n.SubjectLogonId "+
               "WITH n,subjectUser,subjectSession,n.TimeCreated as date "+
               "CREATE (subjectUser)-[usingSession:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(subjectSession) "+
               "SET "+
               "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "usingSession.EventID = n.EventID "+
               "RETURN usingSession";
               
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }


        private async Task SessionCreateGroupUser()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query =
              "MATCH (n:WindowsEvent {EventID : \"4731\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "WHERE n.TargetSid IS NOT NULL "+
              "MERGE (targetGroup:Group {Sid : n.TargetSid}) "+
              "ON CREATE SET  "+
              "targetGroup.Name = n.TargetUserName, "+
              "targetGroup.Sid = n.TargetSid "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "WITH n,targetGroup,subjectSession,n.TimeCreated as date "+
              "CREATE (subjectSession)-[createGroup:CREATE {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(targetGroup) "+
              "SET "+
              "createGroup.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "createGroup.EventID = n.EventID, "+
              "createGroup.NewTargetUserName = n.NewTargetUserName, "+
              "createGroup.OldTargetUserName = n.OldTargetUserName, "+
              "createGroup.TargetDomainName = n.TargetDomainName, "+
              "createGroup.PrivilegeList = n.PrivilegeList "+
              "RETURN createGroup";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }
    }
}
