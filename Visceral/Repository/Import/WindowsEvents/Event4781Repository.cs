using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4781Repository: IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4781Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await UserUsingSession();
            await SessionChangeUsernameUser();
        }

        private async Task UserUsingSession()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query = 
              "MATCH (n:WindowsEvent {EventID : \"4781\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
              "WHERE n.SubjectUserSid IS NOT NULL "+
              "MERGE (subjectUser:User {Sid : n.SubjectUserSid}) "+
              "ON CREATE SET  "+
              "subjectUser.Username = n.SubjectUserName, "+
              "subjectUser.Sid = n.SubjectUserSid, "+
              "subjectUser.DomainName = n.SubjectDomainName "+
              "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
              "ON CREATE SET  "+
              "subjectSession.LogonId = n.SubjectLogonId "+
              "WITH n,subjectUser,subjectSession,n.TimeCreated as date "+
              "CREATE (subjectUser)-[usingSession:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(subjectSession) "+
              "SET "+
              "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
              "usingSession.EventID = n.EventID "+
              "RETURN usingSession";
 
           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }

        private async Task SessionChangeUsernameUser()
        {
           IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
           const string query = 
               "MATCH (n:WindowsEvent {EventID : \"4738\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
               "WHERE n.TargetSid IS NOT NULL "+
               "MERGE (targetUser:User {Sid : n.TargetSid}) "+
               "ON CREATE SET  "+
               "targetUser.Username = n.TargetUserName, "+
               "targetUser.Sid = n.TargetSid, "+
               "targetUser.DomainName = n.TargetDomainName "+
               "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
               "ON CREATE SET  "+
               "subjectSession.LogonId = n.SubjectLogonId "+
               "WITH n,targetUser,subjectSession,n.TimeCreated as date "+
               "CREATE (subjectSession)-[changeUsername:CHANGE {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(targetUser) "+
               "SET "+
               "changeUsername.TimeCreated = custom.date_EvtxToNeo4J(date), "+
               "changeUsername.EventID = n.EventID, "+
               "changeUsername.NewTargetUserName = n.NewTargetUserName, "+
               "changeUsername.OldTargetUserName = n.OldTargetUserName, "+
               "changeUsername.TargetDomainName = n.TargetDomainName, "+
               "changeUsername.PrivilegeList = n.PrivilegeList "+
               "RETURN changeUsername";

           IResultCursor cursor = await session.RunAsync(query);
           await cursor.ConsumeAsync();
        }
    }
}