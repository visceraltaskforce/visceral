using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Repository.Import.WindowsEvents
{
    public class Event4907Repository: IEventWindowsSpecificRepository
    {
        private readonly IDriver _driver;
        
        public Event4907Repository(IDriver driver)
        {
            _driver = driver;
        }

        public async Task Populate()
        {
            await ProcessAsUser();
            await UserUsingSession();
            await SessionUsingHandle();
            await HandleEditObject();
        }

        private async Task ProcessAsUser()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query =
                "MATCH (n:WindowsEvent {EventID : \"4907\", Provider : \"Microsoft-Windows-Security-Auditing\"}) " +
                "MERGE (process:Process {Id : n.ProcessId}) " +
                "ON CREATE SET " +
                "process.Name = n.ProcessName, " +
                "process.Id = n.ProcessId " +
                "MERGE (subjectUser:User { Sid : n.SubjectUserSid }) " +
                "ON CREATE SET " +
                "subjectUser.Sid = n.SubjectUserSid, " +
                "subjectUser.Username = n.SubjectUserName, " +
                "subjectUser.DomainName = n.SubjectDomainName " +
                "WITH n,subjectUser,process,n.TimeCreated as date " +
                "CREATE (process)-[processAs:AS]->(subjectUser) " +
                "SET " +
                "processAs.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "processAs.EventID = n.EventID " +
                "RETURN processAs";

            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }

        private async Task UserUsingSession()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query =
                "MATCH (n:WindowsEvent {EventID : \"4907\", Provider : \"Microsoft-Windows-Security-Auditing\"}) " +
                "MERGE (subjectUser:User {Sid : n.SubjectUserSid}) " +
                "ON CREATE SET  " +
                "subjectUser.Username = n.SubjectUserName, " +
                "subjectUser.Sid = n.SubjectUserSid, " +
                "subjectUser.DomainName = n.SubjectDomainName " +
                "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) " +
                "ON CREATE SET  " +
                "subjectSession.LogonId = n.SubjectLogonId " +
                "WITH n,subjectSession,subjectUser,n.TimeCreated as date " +
                "CREATE (subjectUser)-[usingSession:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(subjectSession) " +
                "SET " +
                "usingSession.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "usingSession.EventID = n.EventID " +
                "RETURN usingSession";

            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }

        private async Task SessionUsingHandle()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query =
                "MATCH (n:WindowsEvent {EventID : \"4907\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
                "MERGE (subjectSession:Session {LogonId : n.SubjectLogonId}) "+
                "ON CREATE SET  "+
                "subjectSession.LogonId = n.SubjectLogonId "+
                "MERGE (handle:Handle {Id : n.HandleId}) "+
                "ON CREATE SET  "+
                "handle.Id = n.HandleId "+
                "WITH n,subjectSession,handle,n.TimeCreated as date "+
                "CREATE (subjectSession)-[usingHandle:USING {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(handle) "+
                "SET "+
                "usingHandle.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "usingHandle.EventID = n.EventID "+
                "RETURN usingHandle";

            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }
        
        private async Task HandleEditObject()
        {
            IAsyncSession session = _driver.AsyncSession(o => o.WithDatabase("neo4j"));
            const string query =
                "MATCH (n:WindowsEvent {EventID : \"4907\", Provider : \"Microsoft-Windows-Security-Auditing\"}) "+
                "MERGE (handle:Handle {Id : n.HandleId}) "+
                "ON CREATE SET  "+
                "handle.Id = n.HandleId "+
                "MERGE (objectAccessed:Object {Name : n.ObjectName}) "+
                "ON CREATE SET  "+
                "objectAccessed.Name = n.ObjectName, "+
                "objectAccessed.Type = n.ObjectType, "+
                "objectAccessed.Sd = n.NewSd "+
                "ON MATCH SET  "+
                "objectAccessed.Sd = n.NewSd "+
                "WITH n,objectAccessed,handle,n.TimeCreated as date "+
                "CREATE (handle)-[editObject:EDIT {TimeCreated : custom.date_EvtxToNeo4J(date)}]->(objectAccessed) "+
                "SET "+
                "editObject.TimeCreated = custom.date_EvtxToNeo4J(date), "+
                "editObject.EventID = n.EventID, "+
                "editObject.NewSd = n.NewSd "+
                "RETURN editObject";

            IResultCursor cursor = await session.RunAsync(query);
            await cursor.ConsumeAsync();
        }
    }
}
