﻿using System.Threading.Tasks;

namespace Visceral.Repository.Import.WindowsEvents
{
    public interface IEventWindowsSpecificRepository
    {
        public Task Populate();
    }
}