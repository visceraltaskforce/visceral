﻿namespace Visceral.Services.API
{
    public class DateService
    {
        /**
         * Transforms a microsecond unix timestamp to a string that will be understood by Neo4J as a Datetime.
         */
        public static string? MicrosecondTimestampToNeo4JDatetimeString(long? timestamp)
        {
            return timestamp == null ? null : $"datetime({{ epochSeconds: {timestamp/1000000}, nanosecond: {(timestamp % 1000000)*1000} }})";
        }
    }
}