using System.Collections.Generic;

namespace Visceral.Services.API
{
    public class LabelService
    {
        private readonly IDictionary<string,object> _errors; 
        private readonly List<IDictionary<string, object>> _errorList;
        private readonly IDictionary<string, object> _data;
        
        public LabelService()
        {
            _data = new Dictionary<string, object>();
            _errorList = new List<IDictionary<string, object>>();
            _errors = new Dictionary<string, object> {{"errors", _errorList}};
        }
        public IDictionary<string,object> ResultToApiFormat(object queryResult)
        {
            _data.Clear();
            _data.Add("data",queryResult);
            return _data;
        }
        
        public IDictionary<string, object> ErrorToApiFormat(IDictionary<string, object> errorDictionary)
        {
            _errorList.Clear();
            _errorList.Add(errorDictionary);
            return _errors;
        }
    }
}