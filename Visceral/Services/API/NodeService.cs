using System;
using System.Collections.Generic;
using System.Linq;
using Neo4j.Driver;

namespace Visceral.Services.API
{
    public class NodeService
    {
        private readonly IDictionary<string, string> _labelToId;
        
        public NodeService()
        {
           _labelToId = new Dictionary<string, string>
           {
               {"Session", "LogonId"},
               {"Computer", "Name"},
               {"Handle", "Id"},
               {"Group", "Sid"},
               {"Object","Name"},
               {"WindowsEvent", "EventRecordID"},
               {"PlasoMftEvent", "EventId"},
           };

        }

        /**
         * Transforms a node result from a Neo4J query to a Node usable with Vis.js library
         */
        public IDictionary<string, object> NodeToVisNode(INode node)
        {
            var label = node.Labels.First();
            var properties = node.Properties;

            IDictionary<string, object> visNode = new Dictionary<string, object>
            {
                { "id", node.Id },
                { "label", LabelToIdValue(label,properties) },
                { "group", label },
                { "properties", properties }
            };
            return visNode;
        }

        private string LabelToIdValue(string label,IReadOnlyDictionary<string,object> properties)
        {
            if (_labelToId.ContainsKey(label))
            {
                return (string) properties[_labelToId[label]];
            }

            switch (label)
            {
                case "User":
                    return properties["Username"] + "@" + properties["DomainName"];
                case "Process":
                    //If name wasn't specified, return only Id
                    if (!properties.ContainsKey("Name")) return properties["Id"] + " - ";
                    //Basic case
                    return properties["Id"] + "|" + properties["Name"];
                default:
                    throw new ArgumentException("Label specified doesn't in the NodeService");
            }
            
        }
    }
}