using System.Collections.Generic;
using System.Linq;
using Neo4j.Driver;

namespace Visceral.Services.API
{
    public class PathService
    {
        private readonly NodeService _nodeService;
        private readonly RelationshipService _relationshipService;

        public PathService(NodeService nodeService, RelationshipService relationshipService)
        {
            _nodeService = nodeService;
            _relationshipService = relationshipService;
        }
        
        public (List<IDictionary<string, object>>, IDictionary<string, object>) PathToVisEdgesAndEndNode(IPath path)
        {
            var visRelationships = path.Relationships.AsParallel()
                .Select(relationship => _relationshipService.RelationshipToVisEdge(relationship)).ToList();
            var visEndNode = _nodeService.NodeToVisNode(path.End);
            
            return (visRelationships,visEndNode);
        }

        public (List<IRelationship>, List<INode>) PathToRelationshipsAndEndNodes(List<IPath> paths)
        {
            List<INode> endNodes = paths.AsParallel().Select(path => path.End).Distinct().ToList();

            List<IRelationship> relationships = paths.AsParallel().Select(path => path.Relationships)
                .SelectMany(flatten => flatten).ToList();
            
            return (relationships,endNodes);
        }
    }
}