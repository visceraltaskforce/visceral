using System;
using System.Collections.Generic;
using Neo4j.Driver;

namespace Visceral.Services.API
{
    public class RelationshipService
    {
        /**
         * Transforms a relationship result from a Neo4J query to an Edge usable with Vis.js library
         */
        public IDictionary<string, object> RelationshipToVisEdge(IRelationship relationship)
        {
            if (!relationship.Properties.ContainsKey("TimeCreated"))
            {
                throw new Exception("Relationships must contain a TimeCreated value");
            }

            ZonedDateTime timeCreated = (ZonedDateTime) relationship.Properties["TimeCreated"]; 
            IDictionary<string,object> visEdge = new Dictionary<string, object>
            {
                { "id", relationship.Id },
                { "from", relationship.StartNodeId },
                { "to", relationship.EndNodeId },
                { "label", relationship.Type },
                { "group", relationship.Type },
                { "arrows", "to" },
                { "timestamp", ZonedDateTimeToTimestamp(timeCreated) },
                { "timestamp_microsecond", Math.Truncate((decimal) timeCreated.Nanosecond/1000) % 1000 },
                { "properties" ,relationship.Properties }
            };
            return visEdge;
        }

        /**
         * ZonedDateTime from Neo4J driver to timestamp in milliseconds
         */
        private long ZonedDateTimeToTimestamp(ZonedDateTime datetime)
        {
            return datetime.ToDateTimeOffset().ToUnixTimeMilliseconds();
        }
    }
}