using System;
using System.Collections.Generic;

namespace Visceral.Services
{
    public class FlagService
    {
        /**
         * Here is the list of the different flag for an ntfs entry in the MTF
         */
        private static readonly Dictionary<string, int> NtfsAttributeFlags =new Dictionary<string, int>
        {
            {"Index view", 536870912},
            {"Directory", 268435456},
            {"Encrypted", 16384},
            {"Content not indexed", 8192},
            {"Offline", 4096},
            {"Compressed", 2048},
            {"Reparse Point", 1024},
            {"Sparse File", 512},
            {"Temporary", 256},
            {"Normal", 128},
            {"Device", 64},
            {"Archive", 32},
            {"System", 4},
            {"Hidden", 2},
            {"Read Only", 1}
        };

        private static readonly Dictionary<string, int> AccessList =new Dictionary<string, int>
        {
            {"ACCESS_SYS_SEC", 16777216},
            {"SYNCHRONIZE", 1048576},
            {"WRITE_OWNER", 524288},
            {"WRITE_DAC", 262144},
            {"READ_CONTROL", 131072},
            {"DELETE", 65536},
            {"WriteAttributes", 256},
            {"ReadAttributes", 128},
            {"DeleteChild", 64},
            {"Execute/Traverse", 32},
            {"WriteEA", 16},
            {"ReadEA", 8},
            {"AppendData ", 4},
            {"WriteData ", 2},
            {"ReadData ", 1}
        };
        

        /**
         * The purpose of this method is to translate int attributes flag of a file into comprehensive language
         */
        public static List<string> GetFileAttributes(Int64 attributes)
        {
            var result = new List<string>();
            var intrr = attributes;
            // iterating over the different flag to get all the attributes
            foreach (var (flag, decimalValue) in NtfsAttributeFlags)
            {
                if (intrr - decimalValue >= 0)
                {
                    result.Add(flag);
                    intrr -= decimalValue;
                }
            }
            
            return result;
        }

        public static List<string> GetAccesList(Int64 attributes)
        {
            var intrr = attributes;
            var result = new List<string>();
            
	    Console.WriteLine("========= Start process flag ==========");
            // iterating over the different flag to get all the attributes
            foreach (var (flag, decimalValue) in AccessList)
            {
		if (intrr - decimalValue >= 0)
                {
		    Console.WriteLine(intrr);
		    Console.WriteLine(decimalValue);
                    result.Add(flag);
                    intrr -= decimalValue;
                }
            }
            Console.WriteLine("============== End process flag =============");
            return result;
        }
    }
}
