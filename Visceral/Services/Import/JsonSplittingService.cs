﻿using System;
using System.Diagnostics;
using Visceral.Models.ConfigFiles;

namespace Visceral.Services.Import
{
    public class JsonSplittingService
    {

        private readonly UploadConfig _uploadConfig;

        public JsonSplittingService(UploadConfig uploadConfig)
        {
            _uploadConfig = uploadConfig;
        }

        public int SplitJsonFile(string filePath, int maxEventByBatch)
        {
            if (filePath == null) throw new ArgumentNullException(nameof(filePath));
            if (maxEventByBatch <= 0) throw new ArgumentOutOfRangeException(nameof(maxEventByBatch));

            JsonLineSplitting(filePath, maxEventByBatch, 6);
            JsonLineToJsonBatches();
            return GetNumberSubFiles();
        }

        private void JsonLineSplitting(string filePath, int linesPerBatch, int suffixLength)
        {
            //-d use numeric suffixes starting at 0, not alphabetic
            var cmd =
                $"split --suffix-length={suffixLength} -d --additional-suffix=.json --lines={linesPerBatch} {filePath} {_uploadConfig.StoredFilePath}split/import";
            Console.WriteLine($"Splitting command : {cmd}");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{cmd}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }

        private void JsonLineToJsonBatches()
        {
            var cmd =
                $"cd {_uploadConfig.StoredFilePath}split; for f in import*; do sed '1s/^/[/; $!s/$/,/; $s/$/]/' $f > ../json/$f; rm $f; done";
            Console.WriteLine($"Converting command : {cmd}");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{cmd}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }
        
        private int GetNumberSubFiles(){
            
            var cmd =
                $"ls {_uploadConfig.StoredFilePath}json|wc -w";
            Console.WriteLine($"Number SubFiles command : {cmd}");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{cmd}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            var result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            
            if (int.TryParse(result, out var subFileNumber))
            {
                return subFileNumber;
            }
            throw new Exception($"Couldn't parse command response : {result}");
        }
    }
}