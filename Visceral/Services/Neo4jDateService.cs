﻿using System;
using System.Globalization;
using Visceral.Models.ErrorsApi;

namespace Visceral.Services
{
    public class Neo4JDateService
    {
        private readonly string _dateFormat;
        
        public Neo4JDateService()
        {
            _dateFormat = "yyyy-MM-dd-HH:mm";
        }
       
        public string FromString(string dateString)
        {
            if (dateString == null) throw new ArgumentNullException(nameof(dateString));
        
            DateTime dateTime;
            try
            {
                dateTime = DateTime.ParseExact(dateString, _dateFormat, CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                throw new BadRequestError()
                    .SetDescription($"Date format ({_dateFormat}) wasn't respected, got {dateString}");
            }
       
            return $"datetime({{ year:{dateTime.Year}, month:{dateTime.Month}, day:{dateTime.Day}, hour:{dateTime.Hour}, minute:{dateTime.Minute} }})";
        }
        
        public string? FromStringOptional(string? dateString)
        {
            if (dateString == null) return null;
            
            DateTime dateTime;
            try
            {
                dateTime = DateTime.ParseExact(dateString, _dateFormat, CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                throw new BadRequestError()
                    .SetDescription($"Date format ({_dateFormat}) wasn't respected, got {dateString}");
            }

            //Console.WriteLine($"date({{ year:{dateTime.Year}, month:{dateTime.Month}, day:{dateTime.Day}, hour:{dateTime.Hour}, minute:{dateTime.Minute} }})");
            return $"datetime({{ year:{dateTime.Year}, month:{dateTime.Month}, day:{dateTime.Day}, hour:{dateTime.Hour}, minute:{dateTime.Minute} }})";
        }
    }
}