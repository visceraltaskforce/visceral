using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Neo4j.Driver;
using Visceral.Models.ConfigFiles;
using Microsoft.OpenApi.Models;
using Visceral.Repository.API;
using Visceral.Repository.Import;
using Visceral.Services.API;
using Visceral.Services.Import;

namespace Visceral
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddSingleton(GraphDatabase.Driver("bolt://neo4j",AuthTokens.Basic("neo4j", "bite" )));
            // Ingestion Singletons
            services.AddSingleton<UploadConfig>();
            services.AddSingleton<PlasoMftEventRepository>();
            services.AddSingleton<WindowsEventRepository>();
            services.AddSingleton<JsonSplittingService>();
            services.AddSingleton<IndexRepository>();
            services.AddSingleton<GraphRepository>();
            //API Singletons
            services.AddSingleton<LabelRepository>();
            services.AddSingleton<NodeRepository>();
            services.AddSingleton<NodeNeighborsRepository>();
            services.AddSingleton<LabelService>();
            services.AddSingleton<NodeService>();
            services.AddSingleton<ApiService>();
            services.AddSingleton<RelationshipService>();
            services.AddSingleton<PathService>();
            services.AddSingleton<DateService>();
            services.AddSingleton<CustomFunctionRepository>();
            
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Visceral API", 
                    Version = "v1",
                    Description = "ASP.NET Core Web API enabling to recover data from neo4j database and get back information for the Vis Network.\n" +
                                  "This API follows the https://jsonapi.org/ specifications.",
                    Contact = new OpenApiContact
                    {
                        Name = "Visceral Taskforce",
                        Email = "visceral_team@protonmail.com",
                        Url = new Uri("https://twitter.com/Visceral_Team")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under GPLv3",
                        Url = new Uri("https://www.gnu.org/licenses/gpl-3.0.en.html"),
                    }
                });
                
                // Add XML comments to the swagger API 
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Visceral V1");
            });
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    "Import",
                    "{controller=Import}/{action=Import}/{id?}");
            });
        }
    }
}