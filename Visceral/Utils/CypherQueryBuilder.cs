﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Neo4j.Driver;

namespace Visceral.Utils
{
    public class CypherQueryBuilder
    {
        private StringBuilder QueryStringBuilder { get; }
        private readonly IDictionary<string, object> _queryParameters;
        private readonly Regex _regParameterName ;
        private readonly IAsyncSession _session;
        private int _whereCounter;
        public enum WhereOperators
        {
            And,
            Or
        };

        private string Query => QueryStringBuilder.ToString();

        public CypherQueryBuilder(IAsyncSession session)
        {
            _session = session;
            QueryStringBuilder = new StringBuilder();
            _queryParameters = new Dictionary<string, object>();
            _regParameterName = new Regex(@"\$(\w+)");
            _whereCounter = 0;
        }

        public CypherQueryBuilder Match(string matchActionString)
        {
            QueryStringBuilder.AppendFormat("MATCH {0} ",matchActionString);
            _whereCounter = 0;
            return this;
        }

        public CypherQueryBuilder WhereDynamicParam(string condition, object parameter, WhereOperators operatorType)
        {
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            if (parameter == null) throw new ArgumentNullException(nameof(parameter));
            if (!Enum.IsDefined(typeof(WhereOperators), operatorType))
                throw new InvalidEnumArgumentException(nameof(operatorType), (int) operatorType,
                    typeof(WhereOperators));
            if (_whereCounter < 0) throw new IndexOutOfRangeException("_whereCounter must be positive");
            
            if (_whereCounter == 0)
            {
                ConstructQueryFromDynamicParameter("WHERE",condition,parameter);
                _whereCounter++;
                return this;
            }

            switch (operatorType)
            {
                case WhereOperators.And:
                    ConstructQueryFromDynamicParameter("AND",condition,parameter);
                    break;
                case WhereOperators.Or:
                    ConstructQueryFromDynamicParameter("OR",condition,parameter);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _whereCounter++; 
            return this;
        }
        
        public CypherQueryBuilder WhereDynamicParamOptional(string condition, object? optionalParameter, 
            WhereOperators operatorType)
        {
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            if (!Enum.IsDefined(typeof(WhereOperators), operatorType))
                throw new InvalidEnumArgumentException(nameof(operatorType), (int) operatorType,
                    typeof(WhereOperators));
            if (_whereCounter < 0) throw new IndexOutOfRangeException("_whereCounter must be positive");

            if (optionalParameter == null) return this;
            
            if (_whereCounter == 0)
            {
                ConstructQueryFromDynamicParameter("WHERE",condition,optionalParameter);
                _whereCounter++;
                return this;
            }
           
            switch (operatorType)
            {
                case WhereOperators.And:
                    ConstructQueryFromDynamicParameter("AND",condition,optionalParameter);
                    break;
                case WhereOperators.Or:
                    ConstructQueryFromDynamicParameter("OR",condition,optionalParameter);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _whereCounter++;
            return this;
        }
        
        public CypherQueryBuilder WhereStatic(string condition, WhereOperators operatorType)
        {
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            if (!Enum.IsDefined(typeof(WhereOperators), operatorType))
                throw new InvalidEnumArgumentException(nameof(operatorType), (int) operatorType,
                    typeof(WhereOperators));
            if (_whereCounter < 0) throw new IndexOutOfRangeException("_whereCounter must be positive");
            
            if (_whereCounter == 0)
            { 
                QueryStringBuilder.AppendFormat("{0} {1} ","WHERE",condition);
                _whereCounter++;
                return this;
            }
            
            switch (operatorType)
            {
                case WhereOperators.And:
                    QueryStringBuilder.AppendFormat("{0} {1} ","AND",condition);
                    break;
                case WhereOperators.Or:
                    QueryStringBuilder.AppendFormat("{0} {1} ","OR",condition);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _whereCounter++;
            return this;
        }
        
        public CypherQueryBuilder WhereStaticOptional(string condition, bool isAdded,WhereOperators operatorType)
        {
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            if (!Enum.IsDefined(typeof(WhereOperators), operatorType))
                throw new InvalidEnumArgumentException(nameof(operatorType), (int) operatorType,
                    typeof(WhereOperators));
            if(_whereCounter < 0) throw new IndexOutOfRangeException("_whereCounter must be positive");
            
            if (!isAdded) return this;
            
            if (_whereCounter == 0)
            {
                QueryStringBuilder.AppendFormat("{0} {1} ","WHERE",condition);
                _whereCounter++;
                return this;
            }
            
            switch (operatorType)
            {
                case WhereOperators.And:
                    QueryStringBuilder.AppendFormat("{0} {1} ","AND",condition);
                    break;
                case WhereOperators.Or:
                    QueryStringBuilder.AppendFormat("{0} {1} ","OR",condition);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _whereCounter++;
            return this;
        }

        public CypherQueryBuilder Return(string returnString)
        {
            QueryStringBuilder.AppendFormat("RETURN {0} ",returnString);
            _whereCounter = 0;
            return this;
        }
        
        public CypherQueryBuilder Skip(int skipValue)
        {
            if(skipValue < 0) throw new ArgumentOutOfRangeException(nameof(skipValue));

            if (skipValue == 0) return this;
            
            QueryStringBuilder.Append("SKIP $Skip ");
            _queryParameters.Add("Skip",skipValue);
            _whereCounter = 0;
            return this;
        }
        
        public CypherQueryBuilder Limit(int limitValue)
        {
            if (limitValue > 0)
            {
                QueryStringBuilder.Append("LIMIT $Limit ");
                _queryParameters.Add("Limit",limitValue);
                _whereCounter = 0;
            }
            return this;
        }
        
        public CypherQueryBuilder Unwind(string unwindActionString)
        {
            QueryStringBuilder.AppendFormat("UNWIND {0} ", unwindActionString);
            _whereCounter = 0;
            return this;
        }

        public CypherQueryBuilder With(string withActionString)
        {
            if (withActionString == null) throw new ArgumentNullException(nameof(withActionString));
            QueryStringBuilder.AppendFormat("WITH {0} ", withActionString);
            _whereCounter = 0;
            return this;
        }

        public CypherQueryBuilder WithOptional(string? withActionStringOptional)
        {
            if(withActionStringOptional == null) return this;
            QueryStringBuilder.AppendFormat("WITH {0} ", withActionStringOptional);
            _whereCounter = 0;
            return this;
        }

        public CypherQueryBuilder Yield(string yieldActionString)
        {
            QueryStringBuilder.AppendFormat("YIELD {0} ",yieldActionString);
            _whereCounter = 0;
            return this;
        }
        
        public CypherQueryBuilder Call(string call)
        {
            QueryStringBuilder.AppendFormat("CALL {0} ", call);
            _whereCounter = 0;
            return this;
        }

        public CypherQueryBuilder CallExpandConfig(string startingNode, string? neighboursLabel, string? relationshipType)
        {
            QueryStringBuilder.AppendFormat("CALL apoc.path.expandConfig({0},{{ ",startingNode); // {{ is used to escape a single { 
            if (neighboursLabel != null) QueryStringBuilder.AppendFormat("labelFilter : \"+{0}\", ", neighboursLabel);  
            if (relationshipType != null) QueryStringBuilder.AppendFormat("relationshipFilter : \"+{0}\", ", relationshipType);  
            QueryStringBuilder.Append("minLevel : 1, maxLevel : 2 }) "); //No need to escape because it's Append and not AppendFormat
            _whereCounter = 0;
            return this;
        }
        
        public async Task<IResultCursor> Exec()
        {
            //Debug
            Console.WriteLine(Query);
            foreach (var queryParametersKey in _queryParameters.Keys)
            {
               Console.WriteLine(queryParametersKey+" "+_queryParameters[queryParametersKey]); 
            }
            //EndDebug
            
            if (_queryParameters.Count == 0)
            {
                return await _session.RunAsync(Query);
            }
            return await _session.RunAsync(Query, _queryParameters);
        }

        private void ConstructQueryFromDynamicParameter(string cypherOperator, string condition, object parameter)
        {
           Match match = _regParameterName.Match(condition);
           if (match.Success)
           {
               string parameterName = match.Groups[1].Value; //Gets the parameter name without the $ (Group[0] = $parameter, Group[1] = parameter)
               QueryStringBuilder.AppendFormat("{0} {1} ", cypherOperator, condition);
               _queryParameters.Add(parameterName,parameter);
           }
           else
           {
               throw new Exception("condition must contain a $ parameter. If your condition doesn't need one, use WhereStatic instead.");
           }
        }
    }
}