/**
* api singleton object giving methods to get data from the C# API
*/
const api = new (class Api{
    
    /**
     * Make a get request on the given api link and returns the data
     * @param url {string} - Url to get from the API
     * @returns {Promise<*>}
     */
    async Get(url){
        if(typeof url !== "string") {
            throw new TypeError(`[api.Get] url parameter must be a string. Given url was ${url} of type ${typeof url}`);
        }
        
        const response = await fetch(url);
        const jsonResponse = await response.json();

        if(jsonResponse.hasOwnProperty("errors")){
            
            throw new Error(jsonResponse.errors);
        }
        if(jsonResponse.hasOwnProperty("data")){
            return jsonResponse.data;
        }
        throw new Error(`[api.Get] data received from the ${url} url doesn't contain an error nor a data property.`);
    }

     /**
     * Query the labels of the database
     * @returns {Promise<Array<label>>} Array of labels
     */
    async queryLabels(){
        return await this.Get("/api/labels");
    }

    /**
     * Make a request on Neo4j returning nodes of a given label. Capped to 300.
     * @param {String} label - label of the nodes you want to get
     * @returns {Promise<Array<Node>>} Array of nodes
     */
    async queryNodesByLabel (label){
        if(typeof label !== "string"){
          throw new Error(`[api.queryNodesByLabel] label parameter must be a string. Given url was ${label} of type ${typeof label}`);  
        } 
        
        return await this.Get(`/api/nodes/?label=${label}&limit=300`);
    }

    /**
     * Query a node from his id and label from the database
     * @param {Object} nodeId - nodeId object returned in a previous functions
     * @returns {Promise<Node>} VisNode of the given id
     */
    async queryNodeById (nodeId){
        if(typeof nodeId !== "number"){
            throw new Error(`[api.queryNodesById] identity parameter must be a number. Given identity was ${nodeId} of type ${typeof nodeId}`);
        }

        return await this.Get(`/api/nodes/${nodeId}`)
    }

    /**
     * Query getting the extended nodes by label from a given id node
     * @param {Number} nodeId - identifier of the selected node
     * @param {String} labelFilter - label of the ending nodes to be extended
     * @param {Number} startDate - starting date of the date filter, value as timestamp in microseconds
     * @param {Number} endDate - ending date of the date filter, value as timestamp in microseconds
     * @returns {Promise<Map<string,Object>>} returns the data from the query or the error
     */
    async queryExtendNodeByLabel(nodeId,labelFilter,startDate, endDate){
        if(typeof nodeId !== "number"){
            throw new Error(`[api.queryNodeByIdExtend] nodeId parameter must be a number. Given identity was ${nodeId} of type ${typeof nodeId}`);
        }
        if(typeof labelFilter !== "string"){
            throw new Error(`[api.queryNodeByIdExtend] labelFilter parameter must be a number. Given identity was ${labelFilter} of type ${typeof labelFilter}`);
        }
        if(startDate && typeof startDate !== "number"){
            throw new Error(`[api.queryNodeByIdExtend] startDate parameter must be a number. Given identity was ${startDate} of type ${typeof startDate}`);
        }
        if(endDate && typeof endDate !== "number"){
            throw new Error(`[api.queryNodeByIdExtend] endDate parameter must be a number. Given identity was ${endDate} of type ${typeof endDate}`);
        }
        
        let api_request = `/api/nodes/${nodeId}/extend/?label=${labelFilter}&limit=100`;
        
        if(startDate) api_request += `&startDate=${startDate}`; 
        if(endDate) api_request += `&endDate=${endDate}`;
        
        console.log(api_request);
        return await this.Get(api_request);
    }

    /**
     * Query the oriented navigation between labels in the database schema
     * @returns {Promise<Map<label,Array<label>>>}
     */
    async queryOrientedNavigationMap(){
        return await this.Get("/api/labels/neighbours/outer/");
    }

    /**
     * Query neighbour labels of a given node selected by its id
     * @param id Id of the selected node
     * @returns {Promise<*>}
     */
    async queryNeighbourLabelsFromNodeId(id){
        return await this.Get(`/api/nodes/${id}/neighbour/labels`);
    }
})();
