// ===== Vis instance, data and options ===== //
let network = {
    instance : null,
	data : {
		nodes: new vis.DataSet([]),
		edges: new vis.DataSet([])
	},
    options : {
		nodes: {
			shape: "dot",
			size: 16,
			font: {
				color: "white"
			}
		},
		edges: {
			smooth: {
				forceDirection: "none"
			}
		},
		physics: {
			barnesHut: {
				gravitationalConstant: -30000,
				centralGravity: 0.05,
				springConstant: 0.005
			},
			minVelocity: 0.75
		}
	}
};

// ===== Vue Instance ===== //
const app = new Vue({
	el: "#app",
	data: {
		general_informations : {
			labels: [],
			labelToId: {
				User: "Username",
				Session: "LogonId",
				Computer: "Name",
				Process: "Name",
				Handle: "Id",
				Group: "Sid",
				WindowsEvent: "EventRecordId",
				PlasoMftEvent: "EventId"
			}
		},
		range_date_filter: {
			display_is_human_readable : false,
			start: {
				date: "",
				time: "",
				seconds_microseconds:"",
				timestamp: null
			},
			end: {
				date: "",
				time: "",
				seconds_microseconds:"",
				timestamp: null,
			}
		},
        menu : {
			information : {
				elt_selected:{
					type: "",
					data: {}
				},
				display_copied_alert : false,
				extend : {
					labels : []
				}
			},	
            first_node_selection : {
				state : "menuStartingLabel",
				start_nodes: [],
				filtered_nodes: [],
				node_filter: "",
				starting_label: "Choose the starting node's type",
				starting_node: "Choose the starting node",
				network_menu_node: {},
			}
		},
		vis: {
			container_id : "vis_network"
		},
		replay_activity :{
		    interval : null,
			is_active : false,
			is_paused : false,
			index : -1,
			timeline : [],
			interval_ms : 1500
		}  
	},
	computed: {
		display_property: function () {
			return this.general_informations.labelToId[this.menu.first_node_selection.starting_label]
		},
		range_start_date: function () {
			return this.strings_to_date("start")
		},
		range_end_date: function () {
			return this.strings_to_date("end")
		},
		is_menu_information_having_elt_selected: function () {
			return this.menu.information.elt_selected.type !== "";
		},
		menu_information_table_caption: function () {
			switch (this.menu.information.elt_selected.type) {
				case "node": return "Node attributes";
				case "edge": return "Relationships attributes";
				default: return "Don't break Visceral please :'( -"
			}
		},
		replay_activity_timeline_is_empty : function () {
			return this.replay_activity.timeline.length === 0; 
		},
		replay_activity_timeline_last_elt_index : function (){
			return this.replay_activity.timeline.length - 1;
		},
		replay_activity_index_is_at_min : function () {
			return this.replay_activity.index === 0;
		},
		replay_activity_index_is_at_max : function(){
			return this.replay_activity.index === this.replay_activity_timeline_last_elt_index;
		},
		range_display_mode_to_text : function () {
			return this.range_date_filter.display_is_human_readable ? "Date" : "Timestamp";
		}
	},
	watch: {
		"menu.first_node_selection.starting_label": async function (newLabel) {
			let nodes = await api.queryNodesByLabel(newLabel);
			//nodes.sort(node => node.label); //Sorting nodes received
			this.menu.first_node_selection.start_nodes = nodes;
			this.menu.first_node_selection.filtered_nodes = nodes;
			this.menu.first_node_selection.state = "menuStartingNode";
		},
		"menu.first_node_selection.node_filter": function (new_filter, old_filter) {
			this.menu.first_node_selection.filtered_nodes = this.menu.first_node_selection.start_nodes.filter((node) => {
				if (node.properties.hasOwnProperty(this.display_property)) {
					return node.properties[this.display_property].includes(this.node_filter)
				}
			})
		},
		"menu.first_node_selection.starting_node": async function (new_starting_node_id) {
			if(new_starting_node_id === undefined) return; //If the starting node is not created yet, stop
			this.menu.first_node_selection.state = "Network";
			const node = await api.queryNodeById(new_starting_node_id);
			network.data.nodes.add(node);
			network.instance = new vis.Network(document.getElementById(this.vis.container_id), network.data, network.options);
			network.instance.on('doubleClick', (evt) => this.double_click_menu_handler(evt));
			network.instance.on('click',this.click_display_info_handler);
		},
		"range_date_filter.display_is_human_readable": function(){
			let elt_selected = this.menu.information.elt_selected;
			let edge = elt_selected.data;
		    if(elt_selected.type === "edge"){
		    	this.menu.information.elt_selected.data.properties.TimeCreated = 
					this.timestamp_to_chosen_format(edge.timestamp,edge.timestamp_microsecond);
			}
		}
	},
	methods: {
		//When element clicked in menu this method is called, and the label of the clicked node is given
		menu_selected_item: function (label) {
			const date_range = {};
			if(this.range_date_filter.display_is_human_readable){
				date_range.start = this.range_start_date;
				date_range.end = this.range_end_date;
			}
			else{
				date_range.start = Number(this.range_date_filter.start.timestamp);
				date_range.end = Number(this.range_date_filter.end.timestamp);
			}
			
			console.log(date_range);

			api.queryExtendNodeByLabel(this.menu.first_node_selection.network_menu_node.id, label,date_range.start,date_range.end).then((result) => {
				//Slower but safer method, if one node/edge already exists, skip it and continue import
				result.nodes.forEach((node)=>{
					try{
						network.data.nodes.add(node);
					}	
					catch(err){
						console.log(err);
					}
				});
				result.edges.forEach((edge)=>{
					try{
						network.data.edges.add(edge);
					}
					catch(err){
						console.log(err);
					}
				});
				
				//If the result is empty, inform the user
				if (result.nodes.length === 0 && result.edges.length === 0) {
					alert(`There weren't any "${label}" objects connected to the selected node.`);
				}

				$(".custom-menu").hide(100); // Hide it AFTER the action was triggered
			});
		},
		strings_to_date: function (type) {
			//If date is given, create it
			if (this.range_date_filter[type].date !== "") {
			    const date = new Date(this.range_date_filter[type].date);				
			    
				//If hour and minutes are given, adds it to the date object
				if(this.range_date_filter[type].time !== ""){
				    const time_splitted = this.range_date_filter[type].time.split(":");
				    date.setHours(Number(time_splitted[0]));
				    date.setMinutes(Number(time_splitted[1]));
				}
				//If seconds and microseconds are given, adds it to the date object
				if(this.range_date_filter[type].seconds_microseconds !== ""){
				    let s_micros = Number(this.range_date_filter[type].seconds_microseconds); 
				    let seconds = Math.trunc(s_micros);
				    let milliseconds = Math.trunc((s_micros % 1).toFixed(6).split(".")[1]/1000);
					let microseconds = (s_micros % 1).toFixed(6).split(".")[1] % 1000;
					date.setSeconds(seconds);
					date.setMilliseconds(milliseconds);
					return date.getTime()*1000 + microseconds;
				}
				return date.getTime()*1000;
			}
			return null;
		},
		timestamp_to_timestamp_microsecond: function(timestamp,timestamp_microsecond){
			return timestamp * 1000 + timestamp_microsecond;
		},
		timestamp_to_human_readable_date: function(timestamp,timestamp_microsecond){
			let dt = new Date(timestamp);
			return `${dt.getFullYear()}-${dt.getMonth()+1}-${dt.getDate()} `
				+ `${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}.${(dt.getUTCMilliseconds()*1000) + timestamp_microsecond}`;
		},
		timestamp_to_chosen_format: function(timestamp,timestamp_microsecond){
			if (this.range_date_filter.display_is_human_readable)
				return this.timestamp_to_human_readable_date(timestamp,timestamp_microsecond);
			else
				return this.timestamp_to_timestamp_microsecond(timestamp,timestamp_microsecond);	
		},
		double_click_menu_handler: async function (evt) {
			if (evt.nodes.length === 1) {//If the double click was on an event
				this.menu.first_node_selection.network_menu_node = network.data.nodes.get(evt.nodes[0]);
				this.menu.information.extend.labels = await api.queryNeighbourLabelsFromNodeId(this.menu.first_node_selection.network_menu_node.id);
				$(".custom-menu").finish().toggle(100);//Display menu
				$(".custom-menu").css({ //Position of the menu
					top: evt.pointer.DOM.y + "px",
					left: evt.pointer.DOM.x + "px"
				});
			}
		},
		click_display_info_handler : function (evt) {
			if(evt.nodes.length === 1){ //Node selected
				const node = network.data.nodes.get(evt.nodes[0]);
				this.menu.information.elt_selected = { type: "node", data: node };
				return;
			}
			if(evt.edges.length === 1){ //Edge selected
				let edge = network.data.edges.get(evt.edges[0]);
				edge.from = network.data.nodes.get(edge.from).label;
				edge.to = network.data.nodes.get(edge.to).label;
				edge.properties.TimeCreated = this.timestamp_to_chosen_format(edge.timestamp,edge.timestamp_microsecond); 
				this.menu.information.elt_selected = { type: "edge", data: edge };
				return;
			}
			//Nothing selected
			this.menu.information.elt_selected = {
				type : "",
				data : {}
			};
		},
        //Creates a temporary input and copies its value in the clipboard
		copy_to_clipboard : function(value) {
			const tmp = document.createElement("input");
			tmp.setAttribute("value", value);
			document.body.appendChild(tmp);
			tmp.select();
			document.execCommand("copy");
			document.body.removeChild(tmp);
			this.menu.information.display_copied_alert = true;
			setTimeout(()=>{this.menu.information.display_copied_alert = false},1000);
		},
		generateActivityTimeline : function (){
			//Deprecated vis code
			let rels = network.data.edges.get({
				fields: ['id', 'timestamp'],
				type: {
					timestamp: 'Date'
				}
			});

			//Non deprecated vis code, but DataPipe isn't in the vis-network library. Waiting for it to be added.
			//const rels = createNewDataPipeFrom(network.data.edges).map(rel =>{ return {id : rel.id, timestamp : new Date(rel.properties.Timestamp)}});

			//Clustering by dates
			let objectClusteredRel = this.groupBy(rels,"timestamp");

			return Object.keys(objectClusteredRel)
				.map(key => objectClusteredRel[key])
				.sort((a, b) =>{
				    timestamp_a_ms = a[0].timestamp * 1000 + a[0].timestamp_microsecond;
					timestamp_b_ms = b[0].timestamp * 1000 + b[0].timestamp_microsecond;
					return timestamp_a_ms - timestamp_b_ms;
				})
				.map(group => group.map(rel => rel.id))
		},
		//Selects consecutively the nodes in the array passed, every X milliseconds
        replay_activity_start : function(){
		    if(this.replay_activity.interval !== null) return;
		    
			if(this.replay_activity.is_paused) {
				this.replay_activity.is_paused = false;
			}
			else{
				this.replay_activity.is_active = true;
				this.replay_activity.timeline = this.generateActivityTimeline();
			}
		    
			this.replay_activity.interval = setInterval(()=>{
				if(this.replay_activity.is_paused) return;
				
				this.replay_activity_next_step();

				if(this.replay_activity_index_is_at_max){
					return this.replay_activity_cancel();
				}
			},this.replay_activity.interval_ms);	
		},
		replay_activity_pause : function () {
			this.replay_activity.is_paused = true;
			clearInterval(this.replay_activity.interval);
			this.replay_activity.interval = null;
		},
		replay_activity_cancel : function () {
			clearInterval(this.replay_activity.interval);
			this.replay_activity.interval = null;
			this.replay_activity.index = -1;
			this.replay_activity.timeline = [];
			this.replay_activity.is_paused = false;
			this.replay_activity.is_active = false;
			network.instance.selectEdges([]);
		},
		replay_activity_prev_step : function () {
			if (this.replay_activity_timeline_is_empty) return;
			
			if((this.replay_activity.index-1) >= 0){
				network.instance.selectEdges(this.replay_activity.timeline[--this.replay_activity.index]);
			}
		},
		replay_activity_next_step : function () {
			if(!this.replay_activity.is_active){
				this.replay_activity.is_active = true;
				this.replay_activity.is_paused = true;
				this.replay_activity.timeline = this.generateActivityTimeline();
			}
			
			if (this.replay_activity_timeline_is_empty) return;

			if((this.replay_activity.index+1) <= this.replay_activity_timeline_last_elt_index){
				network.instance.selectEdges(this.replay_activity.timeline[++this.replay_activity.index]);
			}
		},
		groupBy : function(array,key){
			return array.reduce(function(accumulator, elt) {
				let group = elt[key];
				accumulator[group] = accumulator[group] || [];
				accumulator[group].push(elt);
				return accumulator;
			}, {}); 
		}
	},
    //After vuejs updates the view, add the following listeners
	updated : function () {
		$("td").bind("click",(e) => {
			this.copy_to_clipboard(e.target.textContent)
		});
	}
});

//Getting existing labels from the neo4j database
(async ()=>{
	try{
		app._data.general_informations.labels = await api.queryLabels();
	} catch	(error){
		console.error(error);
	}
})();

// ====== Network Menu ===== //

// If the document is clicked somewhere
$(document).bind("mousedown", function (e) {
	// If the clicked element is not the menu
	if (!$(e.target).parents(".custom-menu").length > 0) {
		// Hide it
		$(".custom-menu").hide(100);
	}
});
